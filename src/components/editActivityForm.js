import {connect} from 'react-redux';

import ActivityForm from './activityForm'
import {editActivity,endEditActivity} from '../reducers/actionCreators'
import {getActivityById} from '../reducers'


const mapStateToProps = (state) => ({
    //activity: getActivityById(state, state.session.editingActivityId)
});


const mapDispatchToProps = {
    onSubmit : editActivity,
    onCancel: endEditActivity,
    onComplete: endEditActivity
}


export default connect(mapStateToProps,mapDispatchToProps)(ActivityForm);