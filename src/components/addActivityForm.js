import {connect} from 'react-redux';

import ActivityForm from './activityForm'
import {addActivity,endAddActivity} from '../reducers/actionCreators'


const mapStateToProps = () => ({
    
});


const mapDispatchToProps = {
    onSubmit : addActivity,
    onCancel: endAddActivity,
    onComplete: endAddActivity
}


export default connect(mapStateToProps,mapDispatchToProps)(ActivityForm);