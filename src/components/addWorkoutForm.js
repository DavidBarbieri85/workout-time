import {connect} from 'react-redux';

import WorkoutForm from './workoutForm'
import {addWorkout,endAddWorkout} from '../reducers/actionCreators'


const mapStateToProps = () => ({
    
});


const mapDispatchToProps = {
    onSubmit : addWorkout,
    onCancel: endAddWorkout,
    onComplete: endAddWorkout
}


export default connect(mapStateToProps,mapDispatchToProps)(WorkoutForm);