import React from "react";
import { connect } from "react-redux";

import { Button } from "../utils/components/inputTypes";
import { startAddActivity } from "../reducers/actionCreators";

const mapDispatchToProps = {
	startAddActivity
};

const ButtonsAddActivity = ({ startAddActivity, workoutId}) => {
	return (
		<div className="add-activity-buttons">
			<Button text="Add a Rest" onClick={() => startAddActivity("Rest", workoutId)} className="addRest" />
			<Button text="Add an Exercise" onClick={() => startAddActivity("Exercise", workoutId)} className="addExercise" />
		</div>
	);
};

export default connect(
	null,
	mapDispatchToProps
)(ButtonsAddActivity);
