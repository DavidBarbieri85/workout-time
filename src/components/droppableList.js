import React from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";

export const DroppableList = ({ id, className, reorderElementsHandler, children }) => {
	const handleDragEnd = result => {
		const { destination, source, draggableId } = result;

		if (!verifyIsValidDrag(destination, source)) {
			return;
		}

		reorderElementsHandler(draggableId, source.index, destination.index);
	};

	const verifyIsValidDrag = (destination, source) => {
		if (!destination || (destination.droppableId === source.droppableId && destination.index === source.index)) {
			return false;
		}
		return true;
	};

	return (
		<DragDropContext onDragEnd={handleDragEnd}>
			<Droppable droppableId={`${id}`}>
				{(provided, snapshot) => (
					<div
						ref={provided.innerRef}
						{...provided.droppableProps}
						className={`${className || ""} ${id}-droppable-list droppable-list ${snapshot.isDraggingOver ? " dragging-over" : ""}`}
					>
						{children}

						{provided.placeholder}
					</div>
				)}
			</Droppable>
		</DragDropContext>
	);
};

export default DroppableList;
