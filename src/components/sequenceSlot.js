import React from "react";
import LabelValue from "../utils/components/labelValue"

const SequenceSlot = ({ slot }) => {
	return (
		<div className={`sequence-slot ${slot.type}`}>
			<div className="container">
				<div className="info">
					<h3 className="name">{slot.name}</h3>
					<div className="description">{slot.description}</div>
				</div>
				<div className="data">
					<LabelValue className="duration" label="duration" value={slot.duration} />
				</div>
			</div>
		</div>
	);
};

export default SequenceSlot;
