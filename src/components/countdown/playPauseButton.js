import React, { Component } from "react";
import * as d3 from "d3";
import styled from "styled-components";

const PlayPauseButtonStyled = styled.div`
	.container {
		width: 100%;
		margin: 0 auto;
	}

	.button {
		padding: 0;
		width: 100%;
		height: 100%;
		border: 0;
		background-color: transparent;
		outline: none;
	}
`;

export class PlayPauseButton extends Component {
	constructor(props) {
		super(props);
		this.animationDuration = 200;
		this.state = {};
	}

	componentDidMount() {
		this.states = {
			playing: {
				nextState: "paused",
				iconEl: this.refs.pauseIcon
			},
			paused: {
				nextState: "playing",
				iconEl: this.refs.playIcon
			}
		};
		this.el = this.refs.jsButton;
		
		let initialIconRef = this.props.isPlaying?"#pause-icon":"#play-icon"
		let stateName = this.el.querySelector(initialIconRef).getAttribute("data-state");
		this.updateState(stateName, () => {
			this.replaceUseEl();
		});
	}

	componentDidUpdate = (prevProps, prevState) => {
		if(this.props.isPlaying !== prevProps.isPlaying){
			this.goToNextState()
		}
	}

	replaceUseEl() {
		d3.select(this.el.querySelector("use")).remove();
		d3.select(this.el.querySelector("svg"))
			.append("path")
			.attr("class", "js-icon")
			.attr("d", this.stateIconPath());
	}

	goToNextState() {
		this.updateState(this.state.nextState, () => {
			d3.select(this.el.querySelector(".js-icon"))
				.transition()
				.duration(this.animationDuration)
				.attr("d", this.stateIconPath());
		});
	}

	updateState(stateName, callback = null) {
		this.setState({ ...this.states[stateName] }, callback);
	}

	stateIconPath() {
		return this.state.iconEl.getAttribute("d");
	}

	render() {
		return (
			<PlayPauseButtonStyled className="play-pause-button">
				<div className="container">
					<button className="button js-button" ref="jsButton">
						<svg width="100%" height="100%" viewBox="0 0 36 36">
							<defs>
								<path
									id="pause-icon"
									ref="pauseIcon"
									data-state="playing"
									d="M11,10 L17,10 17,26 11,26 M20,10 L26,10 26,26 20,26"
								/>
								<path
									id="play-icon"
									ref="playIcon"
									data-state="paused"
									d="M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28"
								/>
							</defs>
						</svg>
					</button>
				</div>
			</PlayPauseButtonStyled>
		);
	}
}

export default PlayPauseButton;
