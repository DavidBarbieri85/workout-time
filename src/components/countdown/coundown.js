import React from "react";
import styled from "styled-components";
import "../../css/countdown.css";
import { convertTenthOfSecondsToTime } from "../../utils/time";
import PlayPauseButton from "./playPauseButton";

const CountdownStyled = styled.div.attrs({
	suffix: props => (props.radius.toString().indexOf("%") === -1 ? "px" : "")
	//strokeLength: props => props.radius * 3.14159 * 2
})`
	position: relative;
	text-align: center;
	width: 100%;
	padding-top: 100%;
	height: 0;
	* {
		box-sizing: border-box;
	}

	.play-pause-button {
		position: absolute;
		margin: 0 10%;
	}

	svg.circles {
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		width: 100%;
		overflow: visible;
	}

	&.paused svg circle {
		animation-play-state: paused !important;
	}

	svg circle {
		display: block;
		width: 100%;
		height: 100%;
		transform-origin: 50% 50%;
		transform: rotateY(-180deg) rotateZ(-90deg);
	}

	svg circle.countdown {
		stroke-dasharray: calc(${props => props.radius} * 3.14159 * 2);
		stroke-dashoffset: calc(${props => props.radius} * 3.14159 * 2);
		stroke-width: ${props => props.strokeWidth + props.suffix};
		stroke: #282c34;
		fill: none;
		animation: countdown ${props => props.time}s linear normal forwards;
	}

	svg circle.countup {
		stroke-dasharray: calc(${props => props.radius} * 3.14159 * 2);
		stroke-dashoffset: calc(-${props => props.radius} * 3.14159 * 2);
		stroke-width: ${props => props.strokeWidth + props.suffix};
		stroke: #e8eaef;
		fill: none;
		animation: countup ${props => props.time}s linear normal forwards;
	}

	.countdown-timer__content {
		position: absolute;
		bottom: 25%;
		left: 20%;
		width: 60%;

		text {
			overflow: visible;
			font-size: 24px;
		}
	}

	@keyframes countup {
		from {
			stroke-dashoffset: calc(-${props => props.radius * 3.14159 * 2});
		}
		to {
			stroke-dashoffset: 0px;
		}
	}

	@keyframes countdown {
		from {
			stroke-dashoffset: 0px;
		}
		to {
			stroke-dashoffset: calc(${props => props.radius * 3.14159 * 2});
		}
	}
`;

export class Countdown extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			timeRemainingInTenthOfSeconds: props.startTimeInSeconds*10,
			isPlaying: false
		};
		this.timer = null;
		this.toggleStartStop = this.toggleStartStop.bind(this);
	}

	decrementTimeRemaining = () => {
		if (this.state.timeRemainingInTenthOfSeconds > 0) {
			this.setState({
				timeRemainingInTenthOfSeconds: this.state.timeRemainingInTenthOfSeconds - 1
			});
		} else {
			clearInterval(this.timer);
		}
	};

	componentDidMount() {
		if(this.state.isPlaying){
			this.startTimer();
		}
	}

	toggleStartStop() {
		this.setState(
			{
				isPlaying: !this.state.isPlaying
			},
			() => {
				if (this.state.isPlaying) {
					this.startTimer();
				} else {
					this.pauseTimer();
				}
			}
		);
	}

	startTimer() {
		this.timer = setInterval(() => {
            this.decrementTimeRemaining();
		}, 100);
        this.props.onStart && this.props.onStart();
	}

	pauseTimer() {
		clearInterval(this.timer);
        this.props.onStop && this.props.onStop();
	}

	render() {
		let { size, strokeWidth, startTimeInSeconds } = this.props;
		let radius = normalizeRadius(size, strokeWidth);

		return (
			<CountdownStyled
				className={`countdown ${this.state.isPlaying ? "playing" : "paused"}`}
				time={startTimeInSeconds}
				size={size}
				strokeWidth={strokeWidth}
				radius={radius}
			>
				<div className="container" onClick={() => this.toggleStartStop()}>
					<svg className="circles">
						<circle className="countup" r={radius} cx="50%" cy="50%" />
						<circle className="countdown" r={radius} cx="50%" cy="50%" />
					</svg>
					<div className="countdown-timer__content">
						<PlayPauseButton isPlaying={this.state.isPlaying} />
						<svg className="text" viewBox="0 0 100 100">
							<text x="50" y="99" textAnchor="middle">
								{convertTenthOfSecondsToTime(this.state.timeRemainingInTenthOfSeconds, (this.state.timeRemainingInTenthOfSeconds < 100))}
							</text>
						</svg>
					</div>
				</div>
			</CountdownStyled>
		);
	}
}

const normalizeRadius = (size, strokeWidth) => {
	let radius;
	if (typeof size === "string" && size.indexOf("%")) {
		radius = parseInt(size.split("%")[0]);
		if (strokeWidth.indexOf("%")) {
			radius = (radius - parseInt(strokeWidth.split("%")[0])) / 2 + "%";
		}
	} else {
		radius = (size - strokeWidth) / 2;
	}
	return radius;
};

export default Countdown;
