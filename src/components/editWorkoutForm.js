import {connect} from 'react-redux';

import WorkoutForm from './workoutForm'
import {editWorkout,endEditWorkout} from '../reducers/actionCreators'
import {getWorkoutById} from '../reducers'


const mapStateToProps = (state) => ({
    workout: getWorkoutById(state, state.session.editingWorkoutId)
});


const mapDispatchToProps = {
    onSubmit : editWorkout,
    onCancel: endEditWorkout,
    onComplete: endEditWorkout
}


export default connect(mapStateToProps,mapDispatchToProps)(WorkoutForm);