import React from "react";
import {Form} from "../utils/components/forms"
import {WorkoutFormContent} from "./forms/workoutFormContent"



export class WorkoutForm extends React.Component {
	constructor(props) {
		super(props);
		this.initialValues = {
			name: "",
			description: "",
			...props.values
		};
	}

	componentDidMount() {
		this.focusInput && this.focusInput.focus()
	}

	render() {
        let isAdding = !this.props.values;

		return (
			<Form
				intitialState={{
					values: this.initialValues,
					submitted: false,
					changedFields: {}
                }}
                validationRules={{
                    name: {required : {message:"Workout name is required"}}
				}}
                mode={{isAdding: isAdding, isEditing:!isAdding}}
                //update={e => this.handleChange(e)}
                onSubmit={data => this.handleSubmit(data)}
                onCancel={() => this.handleCancelWorkoutForm()}
                onComplete={() => this.handleCompleteWorkoutForm()}
                className={"workout-form " + (this.props.className ? this.props.className : "")}
				render={ (props) => {
                        return <WorkoutFormContent {...props} 	ref={input => this.focusInput = input} />
                    }
                }
            />
		)
	}

	handleSubmit(data) {
		console.log("Workout component -> handleSubmit");
		this.props.onSubmit(data);
		this.props.handleCancelClick && this.props.handleCancelClick();
	}

	handleCancelWorkoutForm() {
		console.log("Workout component -> handleCancelWorkoutForm");
		this.props.onCancel();
		this.props.handleSubmitClick && this.props.handleSubmitClick();
	}
	
	handleCompleteWorkoutForm() {
		console.log("Workout component -> handleCompleteWorkoutForm");
		this.props.onComplete && this.props.onComplete();
		this.props.handleComplete && this.props.handleComplete();
	}
}

export default WorkoutForm