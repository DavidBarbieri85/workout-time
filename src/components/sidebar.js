import React from "react";
import { connect } from "react-redux";

import AddWorkoutForm from "../components/addWorkoutForm";
import WorkoutsDroppableList from "../components/workoutsDroppableList";
import ButtonAddWorkout from "../components/buttonAddWorkout";

const mapStateToProps = state => ({
	isAddingWorkout: state.session.isAddingWorkout
});

const Sidebar = (props) => {
	return (
		<div className="side-bar">
			<WorkoutsDroppableList />
			{props.isAddingWorkout ? (
				<div className="add-workout-section">
					<h4> Add workout </h4>
					<AddWorkoutForm/>
				</div>
			) : (
				<div className="add-buttons default-box-padding">
					<ButtonAddWorkout />
				</div>
			)}
		</div>
	);
};

export default connect(mapStateToProps)(Sidebar);
