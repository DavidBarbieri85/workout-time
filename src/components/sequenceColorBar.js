import React from "react";
import {calculateTotalDuration} from '../actions'

const SequenceColorBar = props => {
    let { sequence, direction } = props;
    const _direction = direction === "horizontal" ? "width": "height"

    const totalDuration = calculateTotalDuration(sequence)

	return (
		<div className="color-bar">
			{sequence.map((slot, i) => {
				return <div key={i} className={slot.type} style={{ [_direction]: (slot.duration / totalDuration) * 100 + "%" }} />;
			})}
		</div>
    );
};

export default SequenceColorBar;
