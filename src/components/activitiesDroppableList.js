import React from "react";
import { connect } from "react-redux";

import DroppableList from "./droppableList";
import DraggableElement from "./draggableElement";
import Activity from "./activity";
import { reorderActivities, startEditActivity, deleteActivity } from "../reducers/actionCreators";
import { getActivitiesByWorkoutId } from "../reducers";

const mapStateToProps = (state, ownProps) => ({
	activities: getActivitiesByWorkoutId(state, ownProps.workoutId)
});

const mapDispatchToProps = {
	reorderActivities,
	startEditActivity,
	deleteActivity,
};



export const ActivitiesDroppableList = ({ activities, workoutId, reorderActivities,startEditActivity,deleteActivity }) => {

    const handleReorder = (id, from, to) => {
        reorderActivities(id, from, to ,workoutId)
    }

	return (
		<DroppableList id={"activities"} className="activities-list" reorderElementsHandler={handleReorder}>
			{activities
				? activities.map((activity, i) => {
						return (
							<DraggableElement key={i} id={activity.id} index={i} className={"activity-element"}>
								<Activity activity={activity} onStartEdit={startEditActivity} onDelete={deleteActivity}  />
							</DraggableElement>
						)
				  })
				: "No elements yet"}
		</DroppableList>
	);
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ActivitiesDroppableList);
