import React from "react";
import { connect } from "react-redux";

import ActivitiesDroppableList from "./activitiesDroppableList";
import AddActivityForm from "./addActivityForm";
import { getWorkoutActivitiyIds } from "../reducers";
import ButtonsAddActivity from "./buttonsAddActivity";

const mapStateToProps = (state, ownProps) => ({
	workoutActivities: getWorkoutActivitiyIds(state, ownProps.match.params.id),
	workoutId: ownProps.match.params.id,
	addingActivity: state.session.addingActivity
});

const MainContent = props => {
	const hasActivities = props.workoutActivities.length > 0;
	return (
		<>
			{hasActivities ? (
				<>
					<ActivitiesDroppableList activities={props.workoutActivities} workoutId={props.workoutId} />

					{props.workoutId && props.addingActivity ? (
						<AddActivityForm type={props.addingActivity.type} workoutId={props.workoutId} />
					) : (
						<div className="add-buttons default-box-padding">
							<ButtonsAddActivity workoutId={props.workoutId} />
						</div>
					)}
				</>
			) : (
				<div className="empty-workout">
					<h3>
						You have no activities in this workout yet... <br />
						<br /> Start filling it up!
					</h3>
					<AddActivityForm type={"Exercise"} workoutId={props.workoutId} />
				</div>
			)}
		</>
	);
};

export default connect(mapStateToProps)(MainContent);
