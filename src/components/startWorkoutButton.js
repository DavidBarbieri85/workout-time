

import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

export const StartWorkoutButton = (props) => {
	return (
		<div id="countdown" onClick={() => {props.push("/timer/workout/" + props.match.params.id)}}>
			<div id="countdown-number">START!</div>
		</div>
	);
};

export default connect(null, {push})(StartWorkoutButton)