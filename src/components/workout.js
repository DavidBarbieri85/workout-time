import React from "react";
import { push } from "connected-react-router";
import { connect } from "react-redux";

import { getTotalWorkoutDuration } from "../reducers";
import { convertSecondsToTime } from "../utils/time";
import ActionButtons from "./actionButtons";
import LabelValue from "../utils/components/labelValue";
import EditWorkoutForm from "./editWorkoutForm";

const mapStateToProps = (state, ownProps) => ({
	pathname: state.router.location.pathname,
	isEditing: state.session.editingWorkoutId === ownProps.workout.id,
	editMode: state.session.editMode,
	totalDuration: getTotalWorkoutDuration(state, ownProps.workout.id)
});

const mapDispatchToProps = {
	push
};

const Workout = props => {
	let { workout, isEditing, onStartEdit, onDelete } = props;
	const workoutUrl = `/editor/workout/${props.workout.id}`;
	const isActive = workoutUrl === props.pathname;

	const handleClick = () => {
		props.push(workoutUrl);
	};

	return (
		<div className={`workout ${isActive ? " active" : ""}`} onClick={handleClick}>
			{isEditing ? (
				<EditWorkoutForm values={workout} />
			) : (
				<>
					<div className="container default-box-padding">
						<h3 className="name">{workout.name}</h3>
						<div className="description">{workout.description}</div>
						<div className="duration">
							{workout.activities && workout.activities.length > 0 ? (
								<LabelValue label="Total duration" value={convertSecondsToTime(props.totalDuration)} />
							) : (
								<>No exercises in this workout yet</>
							)}
						</div>
					</div>

                    {/* REFACTOR */}
					{(onStartEdit || onDelete) && (
						<ActionButtons
							onEditButtonClick={e => {
								e.stopPropagation();
								props.onStartEdit(workout.id);
							}}
							onDeleteButtonClick={e => {
								e.stopPropagation();
								props.onDelete(workout.id);
								props.push("/editor");
							}}
						/>
					)}
				</>
			)}
		</div>
	);
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Workout);
