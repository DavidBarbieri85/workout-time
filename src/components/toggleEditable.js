import React from "react";
import { connect } from "react-redux";
import { toggleEditMode } from "../reducers/actionCreators";
import { CheckBox } from "../utils/components/inputTypes";

const mapDispatchToProp = {
	toggleEditMode
};

const mapStateToProps = ({session:{editMode}}) => ({
    editMode
});

export const ToggleEditable = (props) => {
	return <CheckBox label={"toggle editable"} onChange={props.toggleEditMode} value={props.editMode} />;
};

export default connect(
	mapStateToProps,
	mapDispatchToProp
)(ToggleEditable);
