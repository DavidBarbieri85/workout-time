import React from "react";
import { connect } from "react-redux";

import { Button } from "../utils/components/inputTypes";
import {startAddWorkout} from "../reducers/actionCreators"

const mapDispatchToProps = {
	startAddWorkout
}

const ButtonAddWorkout = ({startAddWorkout}) => {
	return <Button text="Add a Workout" onClick={startAddWorkout} className="addWorkout" />;
};

export default connect(
	null,
	mapDispatchToProps
)(ButtonAddWorkout);
