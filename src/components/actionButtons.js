import React from "react";
import { Button } from "../utils/components/inputTypes";

class ActionButtons extends React.Component {
	constructor(props) {
		super(props);
		this.state = { isOpen: false };
	}

	toggle(e) {
		e.stopPropagation();
		this.setState({ isOpen: !this.state.isOpen });
	}

	render() {
		const { onEditButtonClick, onDeleteButtonClick } = this.props;
		return (
			<div className={`action-buttons ${this.state.isOpen && "open"}`}>
				<div className="toggle" onClick={e => this.toggle(e)}>
					<div className="balls">
						<div className="ball top" />
						<div className="ball center" />
						<div className="ball bottom" />
					</div>
				</div>
				{this.state.isOpen && (
					<div className="container">
						<div className="buttons">
							<Button className="edit-lid" onClick={onEditButtonClick} text={"Edit"} />
							<Button className="delete-lid" onClick={onDeleteButtonClick} text={"Delete"} />
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default ActionButtons;
