import React from "react";
import { connect } from "react-redux";

import DroppableList from "./droppableList";
import DraggableElement from "./draggableElement";
import Workout from "./workout";
import { startEditWorkout, deleteWorkout, reorderWorkouts } from "../reducers/actionCreators";
import { getAllWorkouts } from "../reducers";

import { } from "../reducers/actionCreators";

const mapStateToProps = state => ({
	workouts: getAllWorkouts(state)
});

const mapDispatchToProps = {
	reorderWorkouts,
	startEditWorkout,
	deleteWorkout,
};

export const WorkoutsDroppableList = ({ workouts, reorderWorkouts,startEditWorkout,deleteWorkout }) => {
	return (
		<DroppableList id={"workouts"} className="workouts-list" reorderElementsHandler={reorderWorkouts}>
			{workouts
				? workouts.map((workout, i) => {
						return (
							<DraggableElement key={i} id={workout.id} index={i} className={"workout-element"}>
								<Workout workout={workout} onStartEdit={startEditWorkout}  onDelete={deleteWorkout} />
							</DraggableElement>
						);
				  })
				: "No elements yet"}
		</DroppableList>
	);
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WorkoutsDroppableList);
