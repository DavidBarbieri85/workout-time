import React from "react";

import ActionButtons from "./actionButtons";
import LabelValue from "../utils/components/labelValue";
import EditActivityForm from "./editActivityForm";
import SequenceColorBar from "./sequenceColorBar";
import { prepareSingleActivitySequence } from "../actions";
import { connect } from "react-redux";

const mapStateToProps = ({ session }, { activity }) => ({
	isEditing: session.editingActivityId === activity.id,
	editMode: session.editMode
});

const mapDispatchToProps = {
	/* startEditActivity,
	deleteActivity */
};

const Activity = props => {
	let { activity, onStartEdit, onDelete } = props;

	return (
		<div className={`activity ${activity.type}`}>
			{props.isEditing ? (
				<EditActivityForm values={activity} />
			) : (
				<>
					<SequenceColorBar sequence={prepareSingleActivitySequence(activity)} />
					<div className="container default-box-padding">
						<div className="info">
							<h3 className="name">{activity.name}</h3>
							<div className="description">{activity.description}</div>
						</div>
						<div className="data">
							{activity.numSeries ? <LabelValue className="series" label="series" value={activity.numSeries} /> : null}

							<LabelValue className="duration" label="duration" value={activity.duration} />

							{activity.restBetween ? (
								<LabelValue className="rest-between" label="rest between" value={activity.restBetween} />
							) : null}
						</div>

						{/* REFACTOR */}
						{(onStartEdit || onDelete) && (
							<ActionButtons
								onEditButtonClick={e => {
									e.stopPropagation();
									onStartEdit(activity.id);
								}}
								onDeleteButtonClick={e => {
									e.stopPropagation();
									onDelete(props.workoutId, activity.id);
								}}
							/>
						)}
					</div>
				</>
			)}
		</div>
	);
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Activity);
