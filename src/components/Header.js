import React from "react";
import { Route } from "react-router";
import StartWorkoutButton from "./startWorkoutButton"
import ToggleEditable from "./toggleEditable"


export const Header = ({isEditingMode, onToggleEditMode }) => {
	return (
		<header className={"app-header"}>
			<Route path={"/editor/workout/:id"} component={StartWorkoutButton} />
			
			<Route path={"/editor"} component={ToggleEditable} />
			
		</header>
	);
};

export default Header;
