import React from "react"
import {Form} from "../utils/components/forms"
import {ActivityFormContent} from "./forms/activityFormContent"

export default class ActivityForm extends React.Component {
    constructor(props) {
        super(props);
        this.initialValues = {
            type: props.type || "Exercise",
            name: props.type === "Rest"? "Rest": "",
            description:"",
            duration: 0,
            numSeries: 1,
            restBetween:0,
            workoutId: props.workoutId,
            ...props.values
        }
    }

    componentDidMount(){        
		this.focusInput && this.focusInput.focus()
    }
    
    render(){
        let isAdding = !this.props.values;
        
        return(
            <Form
				intitialState={{
					values: this.initialValues,
					submitted: false,
					changedFields: {}
                }}
                validationRules={{
                    name: {
                        required : {message:"Activity name is required"}
                    }/* , ------------ TO IMPLEMENT MIN VALIDATION
                    duration: {
                        min: {value: 1, message: "Insert duration" }
                    },
                    restBetween: {
                        min: {value: 1, message: "Insert rest" }
                    },
                    series: {
                        min: {value: 1, message: "At least 1" }
                    } */
				}}
                mode={{isAdding: isAdding, isEditing:!isAdding}}
                //update={() => {console.log("update from props")}} 
                onSubmit={data => this.handleSubmit(data)}
                onCancel={() => this.handleCancelActivityForm()}
                onComplete={() => this.handleCompleteActivityForm()}
                
                className={"activity-form " + (this.props.className ? this.props.className : "")}
				render={ (props) => {
                        return <ActivityFormContent {...props}	ref={input => this.focusInput = input} />
                    }
                }
            />
        )
    }

    handleSubmit(data) {
        console.log("activityForm component -> handleSubmit");	this.props.onSubmit(data);
		this.props.handleCancelClick && this.props.handleCancelClick();
	
    }

    handleCancelActivityForm(){
		console.log("activityForm component -> handleCancelActivityForm");
        this.props.onCancel();
		this.props.handleSubmitClick && this.props.handleSubmitClick();
	
    }
    handleCompleteActivityForm() {
		console.log("activityForm component -> handleCompleteActivityForm");
        this.props.onComplete && this.props.onComplete();
        this.props.handleComplete && this.props.handleComplete()
	}
}