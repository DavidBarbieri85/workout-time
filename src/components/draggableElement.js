import React from "react";
import { Draggable } from "react-beautiful-dnd";

const DraggableElement = ({ id, index, className, children }) => {
	return (
		<Draggable draggableId={id} index={index}>
			{(provided, snapshot) => (
                <div
                    ref={provided.innerRef}
					{...provided.draggableProps}
					{...provided.dragHandleProps}
					className={`${className} ${id}-draggable-element draggable-element ${snapshot.isDragging ? " dragging" : ""}`}
				>
					{children}
				</div>
			)}
		</Draggable>
	);
};

export default DraggableElement;
