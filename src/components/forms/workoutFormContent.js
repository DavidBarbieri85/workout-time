import React from "react";
import LabelValue from "../../utils/components/labelValue";
import { convertSecondsToTime } from "../../utils/time";
import { Input } from "../../utils/components/inputTypes";
import { Button } from "../../utils/components/inputTypes";

export const WorkoutFormContent = React.forwardRef(
	({ form, className, mode, onChange, handleOnSubmit, handleOnCancel }, ref) => {
		
		const { values, changedFields, submitted, errors } = form;
		const hasErrors = Object.keys(errors).length !== 0;

		return (
			<form className={"workout " + className + " " + (mode.isAdding ? "adding" : "editing")} onSubmit={handleOnSubmit}>
				<div className="container default-box-padding">
					<div className="fields">
						<h3 className="name">
							<Input
								name="name"
								type="text"
								placeholder="Name"
								value={values.name}
								onChange={onChange}
								error={changedFields.name && errors.name}
								innerRef={ref}
							/>
						</h3>

						<div className="description">
							<Input
								name="description"
								type="text"
								placeholder="Description"
								value={values.description}
								onChange={onChange}
								error={changedFields.description && errors.description}
							/>
						</div>

						{!mode.isAdding ? (
							<div className="duration">
								{values.totalDuration && values.totalDuration > 0 ? (
									<LabelValue label="Total duration" value={convertSecondsToTime(values.totalDuration)} />
								) : (
									<>No exercises in this workout yet</>
								)}
							</div>
						) : null}
					</div>

					<div className="buttons flex-space-between">
						<Button text="Cancel" className="cancel" type="button" onClick={handleOnCancel} />
						<Button text="Save" className="save" type="submit" disabled={hasErrors} />
					</div>
				</div>
			</form>
		);
	}
);

export default WorkoutFormContent;
