import React from "react";
import { Input, TimeSpanPicker } from "../../utils/components/inputTypes";
import {Button} from '../../utils/components/inputTypes'

export const ActivityFormContent = React.forwardRef(
	({ form, className, mode, onChange, handleOnSubmit, handleOnCancel }, ref) => {
		
		const { values, changedFields, submitted, errors } = form;
		const hasErrors = Object.keys(errors).length !== 0;

		return (
			<form
				className={"activity " + className + " " + (mode.isAdding ? "adding" : "editing")}
				onSubmit={handleOnSubmit}
			>
				<div className="container default-box-padding">
					<div className="fields">
						<div className="info">
							<h3 className="name">
								<Input
									name="name"
									type="text"
									placeholder="Activity name"
									value={values.name}
									onChange={onChange}
									error={changedFields.name && errors.name}
									innerRef={ref}
								/>
							</h3>

							<div className="description">
								<Input
									name="description"
									type="text"
									placeholder="Description"
									value={values.description}
									onChange={onChange}
									error={changedFields.description && errors.description}
								/>
							</div>
						</div>
						

						{values.type === "Exercise" ? (

							<div className="Series">
								<Input
									label="Series"
									name="numSeries"
									onChange={onChange}
									type="number"
									min={1}
									value={values.numSeries}
								/>
							</div>
							) : null
						}
						<div className="times flex-space-between">
							<div className="picker default-push-right">
								<TimeSpanPicker
									label="Duration"
									name="duration"
									value={values.duration}
									onChange={onChange}
									min={1}
								/>
							</div>
							{values.type === "Exercise" ? (
								<div className="picker">
									<TimeSpanPicker
										label="Rest between"
										name="restBetween"
										value={values.restBetween}
										onChange={onChange}
										min={1}
									/>
								</div>
							) : null}
						</div>
					</div>

					<div className="buttons flex-space-between">
						<Button text="Cancel" className="cancel" type="button" onClick={handleOnCancel}>
						</Button>
						<Button text="Save" className="save" type="submit" disabled={hasErrors}>
						</Button>
					</div>
				</div>
			</form>
		);
	}
);

export default ActivityFormContent;