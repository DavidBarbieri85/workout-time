import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import connectedRootReducer from '../reducers'
//import promise from 'redux-promise'
import logger from 'redux-logger'

//const initialState = Immutable.Map()
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose


const initStore = (history, initialState)  => {
    const store = createStore(
        connectedRootReducer(history), // root reducer with router state
        initialState,
        composeEnhancer(
            applyMiddleware(
                routerMiddleware(history),
                //promise,
                logger 
            // ... other middlewares ...
            ),
        ),
    )
    return store;
}


export default initStore