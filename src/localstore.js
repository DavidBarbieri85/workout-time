export const get = () => {
    try {
        localStorage.getItem('state')
        return JSON.parse(localStorage.getItem('state')) || undefined
    }
    catch(e){
        console.log("failed to load localstorage");
    }
    
    return undefined;
}
export const set = (state, props = null) => {
    try {
        /* let toSave = {};
        props.forEach(p => toSave[p] = state[p]); */
        localStorage.setItem('state',JSON.stringify(state));
    }
    catch (e){
        console.log("failed to save localstorage");
    }
}