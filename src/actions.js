
export function prepareSingleActivitySequence(activity) {
	let activitySequence = []
	if (!activity.numSeries) {
		activitySequence.push({ duration: activity.duration, name: activity.name, description: activity.description, type: "rest" });
		return activitySequence;
	}
	let numberOfSlots = activity.numSeries ? activity.numSeries * 2 - 1 : 1;
	numberOfSlots = activity.restAfter > 0 ? numberOfSlots + 1 : numberOfSlots;

	for (let i = 1; i <= numberOfSlots; i++) {
		let thisSlotDuration = i % 2 !== 0 ? activity.duration : activity.restBetween;
		activitySequence.push({
			duration: thisSlotDuration,
			name: activity.name,
			description: activity.description,
			type: i % 2 !== 0 ? "exercise" : "rest"
		});
	}
	return activitySequence;
}

// return int for total duration of sequence
export function calculateTotalDuration(sequence) {
	let totalDuration = sequence.reduce((acc, activity) => {
		return (acc += parseInt(activity.duration));
	}, 0);

	return totalDuration;
}