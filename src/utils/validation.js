

const validate = (values, validationRules) => {
	if (!validationRules) {
		return {};
	}
	let errors = {};

	for (var valueKey in validationRules) {
		if (validationRules.hasOwnProperty(valueKey)) {
			errors = { ...errors, ...validateField(valueKey, values[valueKey], validationRules[valueKey]) };
		}
		return errors;
	}
};

const validateField = (field, value, rules) => {
	for (var ruleKey in rules) {
		if (rules.hasOwnProperty(ruleKey)) {
			switch (ruleKey) {
				case "required":
					if (!value || value.length === 0) return { [field]: rules.required.message };
					else return null;

				default:
					break;
			}
		}
	}
};

export function sanitizeInputValue(input){
    switch (input.type) {
        case "number":
            let val = parseInt(input.value)
            return isNaN(val)? 0 : val
    
        default:
            return input.value
    }
}

export default validate