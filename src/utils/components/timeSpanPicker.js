import React from "react";
import { convertSecondsToData, convertDataToSeconds } from "../../utils/time";

export class TimeSpanPicker extends React.Component {
	constructor(props) {
		super(props);
		//this.state = {time: props.time, ...convertSecondsToData(props.time)}
		this.handleChange = this.handleChange.bind(this);
        this.syncChange = this.syncChange.bind(this);
        let minTime = Math.max(props.value, this.props.min)
		let timeObj = convertSecondsToData(minTime);
		this.state = {
			...convertSecondsToData(minTime),
			calculatedSeconds: convertDataToSeconds(timeObj),
			//enableMinutes: this.props.enableMinutes ? true : false
		};
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if (this.props.value !== prevProps.value) {
			this.setState({ ...convertSecondsToData(this.props.value) });
		}
	}

	render() {
		let { name, min, max, placeholderMin, placeholderSec } = this.props;
		return (
			<span className="timeSpanPicker">
				<span className="minutes">
					<span className="text">Min</span>
					<input
						name={name + "-minutes"}
						type="number"
						min={0}
						value={this.state.min}
						onChange={this.syncChange}
						placeholder={placeholderMin}
						ref={input => {
							this.minutesInput = input;
						}}
					/>
				</span>
				<span className="seconds">
					<span className="text">Sec</span>
					<input
						name={name + "-seconds"}
						type="number"
						min={0}
						value={this.state.sec}
						onChange={this.syncChange}
						placeholder={placeholderSec}
						ref={input => {
							this.secondsInput = input;
						}}
					/>
				</span>
				<input
					style={{ display: "none" }}
					name={name}
					ref={input => {
						this.timeHiddenInput = input;
					}}
					type="number"
					min={min}
					value={this.state.calculatedSeconds}
					onChange={this.handleChange}
				/>
			</span>
		);
	}

	syncChange(e) {
		this.setState({ min: this.minutesInput.value, sec: this.secondsInput.value });
		var timeObj = { min: parseInt(this.minutesInput.value), sec: parseInt(this.secondsInput.value) };
		setNativeValue(this.timeHiddenInput, convertDataToSeconds(timeObj));
		this.timeHiddenInput.dispatchEvent(new Event("input", { bubbles: true }));
	}

	handleChange(e) {
		this.props.onChange(e);
	}
}

function setNativeValue(element, value) {
	const valueSetter = Object.getOwnPropertyDescriptor(element, "value").set;
	const prototype = Object.getPrototypeOf(element);
	const prototypeValueSetter = Object.getOwnPropertyDescriptor(prototype, "value").set;

	if (valueSetter && valueSetter !== prototypeValueSetter) {
		prototypeValueSetter.call(element, value);
	} else {
		valueSetter.call(element, value);
	}
}
