import React from "react";
import * as picker from "../../utils/components/timeSpanPicker";

export const CheckBox = ({ label, error, onChange, name, value }) => (
	<label htmlFor={name}>
		{label ? <span className="field-label">{label}</span> : null}
		<input name={name} type="checkbox" checked={value} onChange={onChange} />
		{error ? <ErrorMessage errorFor={name}>{error}</ErrorMessage> : null}
	</label>
);

export const Input = ({ type, label, error, onChange, name, value, placeholder, innerRef, min }) => {
	return (
		<label htmlFor={name}>
			{label ? <span className="field-label">{label}</span> : null}
			<input
				placeholder={placeholder}
				name={name}
				ref={input => {
					innerRef && innerRef(input);
				}}
				min={min && min}
				type={type}
				value={value}
				onChange={onChange}
			/>
			{error ? <ErrorMessage errorFor={name}>{error}</ErrorMessage> : null}
		</label>
	);
};

export const TimeSpanPicker = ({ type, label, error, onChange, name, value, min, max, placeholderSec, placeholderMin, innerRef }) => (
	<label htmlFor={name}>
		{label ? <span className="field-label">{label}</span> : null}
		<picker.TimeSpanPicker
			name={name}
			min={min || 0}
			max={max || null}
			onChange={onChange}
			value={value}
			placeholderMin={placeholderMin}
			placeholderSec={placeholderSec}
		/>
		{error ? <ErrorMessage errorFor={name}>{error}</ErrorMessage> : null}
	</label>
);


export const Button = ({ type="button", disabled, text,className, onClick, innerRef, children }) => (
	<button
		type={type}
		ref={input => {
			innerRef && innerRef(input);
		}}
		disabled={disabled}
		onClick={onClick}
		className={`button ${className || ""}`}
	>
		<span className="text">{text}</span><span className="icon"></span>
		{children}
	</button>
);


export const ErrorMessage = ({ errorFor, className, children }) => {
	let classes = " error-for-" + errorFor + " " + (className ? className : "");
	return <div className={"field-error" + classes}>{children}</div>;
};
