import React from 'react'

const LabelValue = (props) => {
    let {label,separator, value, className} = props;

    return (
        <span className={"labelValue " + className}>
            <span className="label">{label}</span>
            <span className="separator">{separator? separator : ": "}</span>
            <span className="value">{value}</span>
        </span>
    )
}

export default LabelValue