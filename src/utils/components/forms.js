import React from "react";
import validate from "../../utils/validation";
//const errors = validate(values, validationRules);
export class Form extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			values: {},
			submitted: false,
			changedFields: {},
			...props.intitialState,
			errors:{}
		};
		this.onChange = this.onChange.bind(this);
		this.handleOnCancel = this.handleOnCancel.bind(this);
		this.handleOnSubmit = this.handleOnSubmit.bind(this);
		this.update = this.update.bind(this);
		this.updateState = this.updateState.bind(this);
		//this.updatePastedFromPropsInComponentCall = this.updatePastedFromPropsInComponentCall.bind(this);
		this.render = this.render.bind(this);
	}

	onChange = event => {
		// taken straight from the official React Docs
		// https://reactjs.org/docs/forms.html#handling-multiple-inputs
		console.log("Form component -> onChange");
		const target = event.target;
		const value = target.type === "checkbox" ? target.checked : target.value;
		const name = target.name;
		this.update(name, value);
	};

	handleOnCancel = event => {
		console.log("Form component -> onCancel");
		event.preventDefault()
		this.props.onCancel && this.props.onCancel();
	};

	handleOnSubmit = event => {
		console.log("Form component -> onSubmit");
		event.preventDefault()
		this.setState({submitted: true});
		this.props.onSubmit && this.props.onSubmit(this.state.values);
		/// check if submit is ok then
		this.props.onComplete();
	};

	update = (name, value) => {
		console.log("Form component -> update");
		
		if (this.props.update) {
			/* this.setState(state => this.props.update(state, name, value)); */
			this.setState(state => this.props.update(state, name, value));
		} else {
			let newValues = { ...this.state.values, [name]: value }

			this.setState(state => ({
				...state,
				values: { ...state.values, [name]: value },
				changedFields: { ...state.changedFields, [name]: true },
				errors:validate(newValues, this.props.validationRules)
			}));
		}
	};

		updateState = fn => {
		console.log("Form component -> updateState");
		this.setState(state => fn(state));
	};

	/* componentDidUpdate (prevProps, prevState){
		if(this.props.intitialState.values.type !== prevProps.intitialState.values.type){
			this.setState(state => ({
				...state,
				values: { 
					...state.values, 
					...this.props.intitialState.values },
			}));
		}
	} */

	render() {
		console.log("Form component -> render");
		return this.props.render({
			...this.props,
			form: this.state,
			onChange: this.onChange,
			udpate: this.update,
			handleOnCancel: this.handleOnCancel,
			handleOnSubmit: this.handleOnSubmit,
			updateState: this.updateState
		});
	}
}