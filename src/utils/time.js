function sanitizeInt(number) {
	return isNaN(parseInt(number)) ? 0 : number;
}

export function convertDataToSeconds({ min, sec }) {
	let result = sanitizeInt(min) * 60 + sanitizeInt(sec);
	return result;
}

export function convertSecondsToData(seconds) {
	let intSeconds = parseInt(seconds);
	let hrs = Math.floor(intSeconds / 3600);
	let min = Math.floor((intSeconds % 3600) / 60);
	let sec = intSeconds % 60;
	return { min, sec, hrs };
}

export function convertSecondsToTime(seconds) {
	let { min, sec, hrs } = convertSecondsToData(seconds);
	var ret = "";

	if (hrs > 0) {
		ret += "" + hrs + ":" + (min < 10 ? "0" : "");
	}

	ret += "" + min + ":" + (sec < 10 ? "0" : "");

	ret += "" + sec;
	return ret;
}

export function convertTenthOfSecondsToTime(tenths, showTenth = false) {
	let tns = 0;
    let seconds = Math.floor(tenths / 10);
	if (showTenth) {
		tns = tenths % 10;
	}
	let { min, sec, hrs } = convertSecondsToData(seconds);
	var ret = "";

	if (hrs > 0) {
		ret += "" + hrs + ":" + (min < 10 ? "0" : "");
	}
	if (min > 0) {
		ret += "" + min + ":" + (sec < 10 ? "0" : "");
	}

	ret += sec;
	if (showTenth) {
		ret += ":" + tns;
	}
	return ret;
}
