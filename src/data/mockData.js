const mockData = {
	session: {},
	workouts: {
		allIds: [1, 2, 3],
		byId: {
			"1": {
				name: "Upper Body Session",
				description: "fatica 1",
				totalDuration: 666, //seconds caclulate from sum of all duration and rests,
                activities: [1, 2, 3],
                id:1
			},

			"2": {
				name: "Core Session",
				description: "fatica 1",
				totalDuration: 333, //seconds caclulate from sum of all duration and rests,
				activities: [4, 5, 6],
                id:2
			},

			"3": {
				name: "Dita",
				description: "fatica 1",
				totalDuration: 111, //seconds caclulate from sum of all duration and rests,
				activities: [7, 8, 9],
                id:3
			}
		},

		orderedIds: [1, 2, 3]
	},
	activities: {
		allIds: [1, 2, 3, 4, 5, 6, 7, 8, 9],
		byId: {
			"1": {
				id: 1,
				type: "Exercise",
				name: "Trazioni",
				description: "Trazioni su tacche 18mm",

				numSeries: 3,
				duration: 10, //seconds
				restBetween: 60, //seconds
				restAfter: 0
			},
			"2": {
				id: 2,
				type: "Rest",
				name: "Riposo",
				duration: 120 //seconds
			},
			"3": {
				id: 3,
				type: "Exercise",
				name: "Flessioni",
				description: "Flessioni con piedi in alto",
				numSeries: 5,
				duration: 10, //seconds
				restBetween: 30, //seconds
				restAfter: 0
			},
			"4": {
				id: 4,
				type: "Exercise",
				name: "Plank",
				description: "Su gomiti",

				numSeries: 3,
				duration: 10, //seconds
				restBetween: 60, //seconds
				restAfter: 0
			},
			"5": {
				id: 5,
				type: "Rest",
				name: "Riposo",
				duration: 120 //seconds
			},
			"6": {
				id: 6,
				type: "Exercise",
				name: "Barchetta",
				description: "Più disteso possibile",
				numSeries: 3,
				duration: 40, //seconds
				restBetween: 20, //seconds
				restAfter: 0
			},
			"7": {
				id: 7,
				type: "Exercise",
				name: "Sospensioni",
				description: "Su tacche 18mm",

				numSeries: 10,
				duration: 7, //seconds
				restBetween: 3, //seconds
				restAfter: 0
			},
			"8": {
				id: 8,
				type: "Rest",
				name: "Riposo",
				duration: 120 //seconds
			},
			"9": {
				id: 9,
				type: "Exercise",
				name: "Sospensioni",
				description: "tacche 8mm",
				numSeries: 5,
				duration: 10, //seconds
				restBetween: 30, //seconds
				restAfter: 0
			}
		},
		byWorkoutId: {
			1: [1,2,3],
			2: [4,5,6],
			3: [7,8,9]
		}
	}
};

export default mockData;
