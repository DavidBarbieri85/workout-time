const mockData = {
	session: {},
	workouts: {
		allIds: [1, 2, 3],
		byId: {
			"1": {
				name: "Upper Body Session",
				description: "fatica 1",
				totalDuration: 666, //seconds caclulate from sum of all duration and rests,
				activities: [1, 2, 3],
				id: 1
			},

			"2": {
				name: "Core Session",
				description: "fatica 1",
				totalDuration: 333, //seconds caclulate from sum of all duration and rests,
				activities: [4, 5, 6],
				id: 2
			},

			"3": {
				name: "Dita",
				description: "fatica 1",
				totalDuration: 111, //seconds caclulate from sum of all duration and rests,
				activities: [7, 8, 9],
				id: 3
			},

			"4": {
				name: "CrossFit",
				description: "circuiti",
				totalDuration: 111, //seconds caclulate from sum of all duration and rests,
				activities: [7],
				id: 4
			}
		},

		orderedIds: [1, 2, 3]
	},
	activities: {
		allIds: [1, 2, 3, 4, 5, 6, 7, 8, 9],
		byId: {
			"1": {
				id: 1,
				type: "series-activity",
				name: "Trazioni",
				description: "Trazioni su tacche 18mm",
				slots: [1, 2],
				numSeries: 3,
				duration: 10, //seconds
				restBetween: 60, //seconds
				restAfter: 0
			},
			"2": {
				id: 2,
				type: "rest",
				name: "Riposo",
				slots: [3]
			},
			"3": {
				id: 3,
				type: "series-activity",
				name: "Flessioni",
				description: "Flessioni con piedi in alto",
				slots: [4, 5],
				numSeries: 5,
				duration: 10, //seconds
				restBetween: 30, //seconds
				restAfter: 0
			},
			"4": {
				id: 4,
				type: "series-activity",
				name: "Plank",
				description: "Su gomiti",
				slots: [6, 7],
				numSeries: 3,
				duration: 10, //seconds
				restBetween: 60, //seconds
				restAfter: 0
			},
			"5": {
				id: 5,
				type: "rest",
				name: "Riposo",
				slots: [8],
				duration: 120 //seconds
			},
			"6": {
				id: 6,
				type: "series-activity",
				name: "Barchetta",
				description: "Più disteso possibile",
				slots: [9, 10],
				numSeries: 3,
				duration: 40, //seconds
				restBetween: 20, //seconds
				restAfter: 0
			},
			"7": {
				id: 7,
				type: "series-activity",
				name: "Sospensioni",
				description: "Su tacche 18mm",
				slots: [11, 12],

				numSeries: 10,
				duration: 7, //seconds
				restBetween: 3, //seconds
				restAfter: 0
			},
			"8": {
				id: 8,
				type: "rest",
				name: "Riposo",
				slots: [13],
				duration: 120 //seconds
			},
			"9": {
				id: 9,
				type: "series-activity",
				name: "Sospensioni",
				description: "tacche 8mm",
				slots: [14, 15],
				numSeries: 5,
				duration: 10, //seconds
				restBetween: 30, //seconds
				restAfter: 0
			},
			"10": {
				id: 10,
				type: "circuit",
				name: "Circuito 1",
				description: "Circuito crossfit 1",
				slots: [16, 17, 18, 19],
				numSeries: 0,
				duration: 0, //seconds
				restBetween: 0, //seconds
				restAfter: 0
			}
		},

		byWorkoutId: {
			1: [1, 2, 3],
			2: [4, 5, 6],
			3: [7, 8, 9],
			4: [10]
		}
	},

	slots: {
		allIds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
		byId: {
			"1": {
				id: 1,
				type: "duration-exercise",
				name: "Trazioni",
				description: "Trazioni su tacche 18mm",
				duration: 10
			},
			"2": {
				id: 2,
				type: "rest",
				name: "Riposo",
				duration: 60 //seconds
			},
			"3": {
				id: 3,
				type: "rest",
				name: "Riposo",
				duration: 120 //seconds
			},
			"4": {
				id: 4,
				type: "duration-exercise",
				name: "Flessioni",
				description: "Flessioni con piedi in alto",
				duration: 10 //seconds
			},
			"5": {
				id: 5,
				type: "rest",
				name: "Riposo",
				duration: 30 //seconds
			},
			"6": {
				id: 6,
				type: "duration-exercise",
				name: "Plank",
				description: "Su gomiti",
				duration: 10 //seconds
			},
			"7": {
				id: 7,
				type: "rest",
				name: "Riposo",
				duration: 60 //seconds
			},
			"8": {
				id: 8,
				type: "rest",
				name: "Riposo",
				duration: 120 //seconds
			},
			"9": {
				id: 9,
				type: "duration-exercise",
				name: "Barchetta",
				description: "Più disteso possibile",
				duration: 40 //seconds
			},
			"10": {
				id: 10,
				type: "rest",
				name: "Riposo",
				duration: 40 //seconds
			},
			"11": {
				id: 11,
				type: "duration-exercise",
				name: "Sospensioni",
				description: "Su tacche 18mm",
				duration: 7 //seconds
			},
			"12": {
				id: 12,
				type: "rest",
				name: "Riposo",
				duration: 3 //seconds
			},
			"13": {
				id: 13,
				type: "rest",
				name: "Riposo",
				duration: 120 //seconds
			},
			"14": {
				id: 14,
				type: "duration-exercise",
				name: "Sospensioni",
				description: "tacche 8mm",
				duration: 10 //seconds
			},
			"15": {
				id: 15,
				type: "rest",
				name: "Riposo",
				duration: 30 //seconds
			},
			"16": {
				id: 16,
				type: "duration-exercise",
				name: "Sollevamenti col bilanciere",
				description: "",
				duration: 40 //seconds
			},
			"17": {
				id: 17,
				type: "duration-exercise",
				name: "Flessioni a testa in giù",
				description: "",
				duration: 30 //seconds
			},
			"18": {
				id: 18,
				type: "duration-exercise",
				name: "Kettle swing",
				description: "",
				duration: 20 //seconds
			},
			"19": {
				id: 14,
				type: "duration-exercise",
				name: "Pullup",
				description: "",
				duration: 50 //seconds
			}
		},

		idsByActivityId: {
			1: [1, 2],
			2: [3],
			3: [4, 5],
			4: [6, 7],
			5: [8],
			6: [9, 10],
			7: [11, 12],
			8: [13],
			9: [14, 15],
			10: [16, 17, 18, 19]
		}
	}
};

export default mockData;
