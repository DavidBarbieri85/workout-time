

import * as actionCreators from "./actionCreators"
import {SELECT_WORKOUT,START_ADD_ACTIVITY,END_ADD_ACTIVITY,START_ADD_WORKOUT,END_ADD_WORKOUT,START_EDIT_WORKOUT,END_EDIT_WORKOUT, START_EDIT_ACTIVITY, END_EDIT_ACTIVITY, TOGGLE_EDIT_MODE} from "./actionTypes"


jest.mock('uuid', () => {
    return {
        v4: jest.fn(() => 1)
    };
});


describe("toggleEditMode action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: TOGGLE_EDIT_MODE
		};

		expect(actionCreators.toggleEditMode()).toEqual(expectedAction);
	});
});

describe("selectWorkout action creator", () => {
	it("should return action object", () => {
        const data = 1

		const expectedAction  = {
			type: SELECT_WORKOUT,
			id: 1
		};

		expect(actionCreators.selectWorkout(data)).toEqual(expectedAction);
	});
});

describe("startAddActivity action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: START_ADD_ACTIVITY
		};

		expect(actionCreators.startAddActivity()).toEqual(expectedAction);
	});
});

describe("startEditActivity action creator", () => {
	it("should return action object", () => {
        const id=1;

		const expectedAction  = {
			type: START_EDIT_ACTIVITY,
            id: 1
		};

		expect(actionCreators.startEditActivity(id)).toEqual(expectedAction);
	});
});


describe("endAddActivity action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: END_ADD_ACTIVITY
		};

		expect(actionCreators.endAddActivity()).toEqual(expectedAction);
	});
});



describe("endEditActivity action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: END_EDIT_ACTIVITY
		};

		expect(actionCreators.endEditActivity()).toEqual(expectedAction);
	});
});


describe("startAddWorkout action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: START_ADD_WORKOUT
		};

		expect(actionCreators.startAddWorkout()).toEqual(expectedAction);
	});
});

describe("startEditWorkout action creator", () => {
	it("should return action object", () => {
        const id=1;

		const expectedAction  = {
			type: START_EDIT_WORKOUT,
            id: 1
		};

		expect(actionCreators.startEditWorkout(id)).toEqual(expectedAction);
	});
});


describe("endAddWorkout action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: END_ADD_WORKOUT
		};

		expect(actionCreators.endAddWorkout()).toEqual(expectedAction);
	});
});



describe("endEditWorkout action creator", () => {
	it("should return action object", () => {

		const expectedAction  = {
			type: END_EDIT_WORKOUT
		};

		expect(actionCreators.endEditWorkout()).toEqual(expectedAction);
	});
});