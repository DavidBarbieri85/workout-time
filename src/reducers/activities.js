import { combineReducers } from "redux";
import {
	ADD_ACTIVITY,
	EDIT_ACTIVITY,
	DELETE_ACTIVITY,
	DELETE_ACTIVITIES,
	DELETE_WORKOUT,
	ADD_WORKOUT,
	REORDER_ACTIVITIES
} from "./actionTypes";

const activity = (state = {}, action) => {
	switch (action.type) {
		case ADD_ACTIVITY:
		case EDIT_ACTIVITY:
			return {
				...state,
				id: action.id,
				...action.payload
			};
		default:
			return state;
	}
};

export const allIds = (state = [], action) => {
	switch (action.type) {
		case ADD_ACTIVITY:
			return [...state, action.id];

		case DELETE_ACTIVITY:
			const indexOfRemoving = state.indexOf(action.id);
			return [...state.slice(0, indexOfRemoving), ...state.slice(indexOfRemoving + 1)];
		case DELETE_ACTIVITIES:
			return state.filter(id => !action.ids.includes(id));
		default:
			return state;
	}
};

export const byId = (state = {}, action) => {
	switch (action.type) {
		case ADD_ACTIVITY:
		case EDIT_ACTIVITY:
			return {
				...state,
				[action.id]: activity(state[action.id], action)
			};
		case DELETE_ACTIVITY:
			const { [action.id]: value, ...withoutKey } = state;
			return withoutKey;
		case DELETE_ACTIVITIES:
			let newState = action.ids.reduce((iterState, id) => {
				const { [id]: value, ...withoutKey } = iterState;
				return withoutKey;
			}, state);
			return newState;
		default:
			return state;
	}
};

export const byWorkoutId = (state = {}, action) => {
	switch (action.type) {
		case ADD_ACTIVITY:
		case DELETE_ACTIVITY:
			return { ...state, [action.workoutId]: idsByWorkoutId(state[action.workoutId], action) };
		case ADD_WORKOUT:
			return { ...state, [action.id]: idsByWorkoutId(undefined, action) };
		case DELETE_WORKOUT:
			const { [action.workoutId]: value, ...withoutKey } = state;
			return withoutKey;
		case REORDER_ACTIVITIES:
			return {
				...state,
				[action.workoutId]: idsByWorkoutId(state[action.workoutId], action)
			};
		default:
			return state;
	}
};

const idsByWorkoutId = (state = [], action) => {
	switch (action.type) {
		case ADD_ACTIVITY:
			return [...state, action.id];

		case DELETE_ACTIVITY:
			const indexOfRemoving = state.indexOf(action.id);
			return [...state.slice(0, indexOfRemoving), ...state.slice(indexOfRemoving + 1)];

		case REORDER_ACTIVITIES:
			if (state.indexOf(action.activityIdToMove) === -1) return state;

			let newState = [...state];
			newState.splice(action.from, 1);
			newState.splice(action.to, 0, action.activityIdToMove);
			return newState;

		case ADD_WORKOUT:
			return [];

		default:
			return state;
	}
};

const activities = combineReducers({ byId, allIds, byWorkoutId });

// SELECTORS ------------------------------

export const getActivitiesByIds = (state, ids) => {
	return ids.map(id => {
		return state.byId[id];
	});
};

export const getActivitiesByWorkoutId = (state, workoutId) => {
	return state.byWorkoutId[workoutId].map(id => {
		return state.byId[id];
	});
};

// to be moved

export function prepareActivitiesSequence(store) {
	let sequence = store.reduce((acc, activity) => {
		const activitySequence = prepareSingleActivitySequence(activity);
		return acc.concat(activitySequence);
	}, []);

	return sequence;
}

export function prepareSingleActivitySequence(activity) {
	let activitySequence = [];
	if (!activity.numSeries) {
		activitySequence.push({ duration: activity.duration, name: activity.name, description: activity.description, type: "rest" });
		return activitySequence;
	}
	let numberOfSlots = activity.numSeries ? activity.numSeries * 2 - 1 : 1;
	numberOfSlots = activity.restAfter > 0 ? numberOfSlots + 1 : numberOfSlots;

	for (let i = 1; i <= numberOfSlots; i++) {
		let thisSlotDuration = i % 2 !== 0 ? activity.duration : activity.restBetween;
		activitySequence.push({
			duration: thisSlotDuration,
			name: activity.name,
			description: activity.description,
			type: i % 2 !== 0 ? "exercise" : "rest"
		});
	}
	return activitySequence;
}

export default activities;
