import { combineReducers } from "redux";
import {
	ADD_WORKOUT,
	EDIT_WORKOUT,
	DELETE_WORKOUT,
	REORDER_WORKOUTS,
	ADD_ACTIVITY,
	DELETE_ACTIVITY,
	REORDER_ACTIVITIES
} from "./actionTypes";

const workout = (state = {}, action) => {
	switch (action.type) {
		case ADD_WORKOUT:
		case EDIT_WORKOUT:
			return {
				...state,
				id: action.id,
				...action.payload
			};
		case DELETE_ACTIVITY:
			return {
				...state,
				activities: activities(state.activities, action)
			};
		case ADD_ACTIVITY:
			return {
				...state,
				activities: activities(state.activities, action)
			};
		case REORDER_ACTIVITIES:
			return {
				...state,
				activities: activities(state.activities, action)
			};
		default:
			return state;
	}
};

const activities = (state = [], action) => {
	switch (action.type) {
		case REORDER_ACTIVITIES:
			if (state.indexOf(action.activityIdToMove) === -1) return state;

			let newState = [...state];
			newState.splice(action.from, 1);
			newState.splice(action.to, 0, action.activityIdToMove);
			return newState;
		case ADD_ACTIVITY:
			return state.concat([action.id])
		case DELETE_ACTIVITY:
			const indexOfRemoving = state.indexOf(action.id);
			return [...state.slice(0, indexOfRemoving), ...state.slice(indexOfRemoving + 1)];
		default:
			return state;
	}
};

export const allIds = (state = [], action) => {
	switch (action.type) {
		case ADD_WORKOUT:
			return [...state, action.id];

		case DELETE_WORKOUT:
			const indexOfRemoving = state.indexOf(action.id);
			return [...state.slice(0, indexOfRemoving), ...state.slice(indexOfRemoving + 1)];

		default:
			return state;
	}
};

export const byId = (state = {}, action) => {
	switch (action.type) {
		case ADD_WORKOUT:
		case EDIT_WORKOUT:
			return {
				...state,
				[action.id]: workout(state[action.id], action)
			};
		case DELETE_WORKOUT:
			const { [action.id]: value, ...withoutKey } = state;
			return withoutKey;

		case ADD_ACTIVITY:
			return {
				...state,
				[action.workoutId]: workout(state[action.workoutId], action)
			};

		case DELETE_ACTIVITY:
			return {
				...state,
				[action.workoutId]: workout(state[action.workoutId], action)
			};

		case REORDER_ACTIVITIES:
			return {
				...state,
				[action.workoutId]: workout(state[action.workoutId], action)
			};
		default:
			return state;
	}
};

export const orderedIds = (state = [], action) => {
	switch (action.type) {
		case ADD_WORKOUT:
			return [...state, action.id];

		case DELETE_WORKOUT:
			const indexOfRemoving = state.indexOf(action.id);
			return [...state.slice(0, indexOfRemoving), ...state.slice(indexOfRemoving + 1)];

		case REORDER_WORKOUTS:
			let newState = [...state];
			newState.splice(action.from, 1);
			newState.splice(action.to, 0, action.id);
			return newState;

		default:
			return state;
	}
};

// SELECTORS ------------------------------

export function getAllWorkouts(state) {
	var allworkouts = state && state.orderedIds.map(id => ({ ...state.byId[id], id }));
	return allworkouts || [];
}

export function getWorkoutById(state, workoutId) {
	return (state && state.byId[workoutId]) || {};
}

export function getWorkoutActivitiyIds(state, workoutId) {
	return (state && state.byId[workoutId] && state.byId[workoutId].activities) || [];
}

// -----------------------------------------

const workouts = combineReducers({ byId, allIds, orderedIds });
export default workouts;
