

import {addWorkout,editWorkout, deleteWorkout, reorderWorkouts, reorderActivities} from "./actionCreators"
import {ADD_WORKOUT,EDIT_WORKOUT,DELETE_WORKOUT,REORDER_WORKOUTS,REORDER_ACTIVITIES} from "./actionTypes"
import {ADD_ACTIVITY,EDIT_ACTIVITY,DELETE_ACTIVITY,DELETE_ACTIVITIES} from "./actionTypes"



jest.mock('uuid', () => {
    return {
        v4: jest.fn(() => 1)
    };
});

describe("addWorkout action creator", () => {
	it("should return action object", () => {
        const data = {
            name: "Workout 1",
            description: "description"
        };

		const expectedAction  = {
			type: ADD_WORKOUT,
            id: 1,
            payload: {
				name: "Workout 1",
				description: "description"
			}
		};

		expect(addWorkout(data)).toEqual(expectedAction);
	});
});

describe("editWorkout action creator", () => {
	it("should return action object", () => {
        const data = {
            name: "Workout 1",
			description: "description",
			id: 1
        };

		const expectedAction  = {
			type: EDIT_WORKOUT,
            id: 1,
            payload: {
				name: "Workout 1",
				description: "description",
				id:1,
			}
		};

		expect(editWorkout(data)).toEqual(expectedAction);
	});
});

describe("deleteWorkout action creator", () => {
	it("should return action object", () => {
        const id=1;

		const expectedAction  = {
			type: DELETE_WORKOUT,
            id: 1
		};

		expect(deleteWorkout(id)).toEqual(expectedAction);
	});
});

describe("reorderWorkouts action creator", () => {
	it("should return action object", () => {
        const id=1;
        const from = 2;
        const to = 3;

		const expectedAction  = {
			type: REORDER_WORKOUTS,
            id: 1,
            from: 2,
            to: 3
		};

		expect(reorderWorkouts(id, from, to)).toEqual(expectedAction);
	});
});

describe("reorderActivities action creator", () => {
	it("should return action object", () => {
        const workoutId = 1;
        const from = 2;
        const to = 3;
        const activityIdToMove = 5;

		const expectedAction  = {
			type: REORDER_ACTIVITIES,
            workoutId: 1,
            activityIdToMove:5,
            from: 2,
            to: 3
		};

		expect(reorderActivities(activityIdToMove, from, to, workoutId)).toEqual(expectedAction);
	});
});
