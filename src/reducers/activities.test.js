import expect from "expect";
import deepFreeze from "deep-freeze";
import { allIds, byId } from "./activities";
import {ADD_ACTIVITY,EDIT_ACTIVITY,DELETE_ACTIVITY,DELETE_ACTIVITIES} from "./actionTypes"


describe("allIds reducer", () => {
	it("should return the initial state", () => {
		expect(allIds(undefined, {})).toEqual([]);
	});
	it("should handle ADD_ACTIVITY", () => {
		const stateBefore = [];
		const action = {
			type: ADD_ACTIVITY,
			id: 1,
			payload: {
				name: "Activity 1",
				description: "description"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle EDIT_ACTIVITY", () => {
		const stateBefore = [1];
		const action = {
			type: EDIT_ACTIVITY,
			id: 1,
			payload: {
				name: "Activity 1 after",
				description: "description 1 after"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_ACTIVITY", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: DELETE_ACTIVITY,
			id: 2
		};
		const stateAfter = [1, 3];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle DELETE_ACTIVITIES", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: DELETE_ACTIVITIES,
			ids: [2,3]
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
});

describe("byId reducer", () => {
	it("should return the initial state", () => {
		expect(byId(undefined, {})).toEqual({});
	});
	it("should handle ADD_ACTIVITY", () => {
		const stateBefore = {};
		const action = {
			type: ADD_ACTIVITY,
			id: 1,
			payload: {
				name: "Activity 1",
				description: "description"
			}
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Activity 1",
				description: "description"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle EDIT_ACTIVITY", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Activity 1",
				description: "description"
			}
		};
		const action = {
			type: EDIT_ACTIVITY,
			id: 1,
			payload: {
				name: "Activity 1 after",
				description: "description 1 after"
			}
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Activity 1 after",
				description: "description 1 after"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_ACTIVITY", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Activity 1"
			},
			2: {
				id: 2,
				name: "Activity 2"
			},
			3: {
				id: 3,
				name: "Activity 3"
			}
		};
		const action = {
			type: DELETE_ACTIVITY,
			id: 2
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Activity 1"
			},
			3: {
				id: 3,
				name: "Activity 3"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle DELETE_ACTIVITIES", () => {

		const stateBefore = {
			1: {
				id: 1,
				name: "Activity 1"
			},
			2: {
				id: 2,
				name: "Activity 2"
			},
			3: {
				id: 3,
				name: "Activity 3"
			}
		};
		const action = {
			type: DELETE_ACTIVITIES,
			ids: [2,3]
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Activity 1"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});

});
