

import {addActivity,editActivity, deleteActivity, deleteActivities} from "./actionCreators"
import {ADD_ACTIVITY,EDIT_ACTIVITY,DELETE_ACTIVITY,DELETE_ACTIVITIES} from "./actionTypes"



jest.mock('uuid', () => {
    return {
        v4: jest.fn(() => 1)
    };
});

describe("addActivity action creator", () => {
	it("should return action object", () => {
        const data = {
            name: "Activity 1",
			description: "description",
			workoutId:2
        };

		const expectedAction  = {
			type: ADD_ACTIVITY,
			id: 1,
			workoutId:2,
            payload: {
				name: "Activity 1",
				description: "description",
				workoutId:2
			}
		};

		expect(addActivity(data)).toEqual(expectedAction);
	});
});

describe("editActivity action creator", () => {
	it("should return action object", () => {
        const data = {
            name: "Activity 1",
			description: "description",
			id:2
        };

		const expectedAction  = {
			type: EDIT_ACTIVITY,
            id: 2,
            payload: {
				name: "Activity 1",
				description: "description",
				id:2
			}
		};

		expect(editActivity(data)).toEqual(expectedAction);
	});
});

describe("deleteActivity action creator", () => {
	it("should return action object", () => {
		const workoutId = 2;
        const id = 1;

		const expectedAction  = {
			type: DELETE_ACTIVITY,
			id: 1,
			workoutId: 2
		};

		expect(deleteActivity(workoutId,id)).toEqual(expectedAction);
	});
});


describe("deleteActivities action creator", () => {
	it("should return action object", () => {
        const ids=[1,2,3];

		const expectedAction  = {
			type: DELETE_ACTIVITIES,
            ids: [1,2,3]
		};

		expect(deleteActivities(ids)).toEqual(expectedAction);
	});
});