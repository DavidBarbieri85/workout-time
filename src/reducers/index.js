import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import activities, * as activitiesSelectors from "./activities";
import workouts, * as workoutSelectors from "./workouts";
import slots, * as slotsSelectors from "./slots";
import session from "./session";
import {prepareSingleActivitySequence,calculateTotalDuration} from "../actions"

const connectedRootReducer = history =>
	combineReducers({
		router: connectRouter(history),
		activities,
        workouts,
        slots,
		session
	});

// WORKOUT SELECTORS

export function getAllWorkouts(state) {
	return workoutSelectors.getAllWorkouts(state.workouts);
}

export function getWorkoutById(state, workoutId) {
	return workoutSelectors.getWorkoutById(state.workouts, workoutId);
}

export function getWorkoutActivitiyIds(state, workoutId) {
	return workoutSelectors.getWorkoutActivitiyIds(state.workouts, workoutId);
}

// ACTIVITIES SELECTORS

export function getActivitiesByIds(state, ids) {
	return activitiesSelectors.getActivitiesByIds(state.activities, ids);
}

export function getActivitiesByWorkoutId(state, workoutId) {
	return activitiesSelectors.getActivitiesByWorkoutId(state.activities, workoutId);
}


// SLOTS SELECTORS

export function getSlotsByIds(state, ids) {
	return slotsSelectors.getSlotsByIds(state.slots, ids);
}

export function getSlotsByActivityId(state, activityId) {
	return slotsSelectors.getSlotsByActivityId(state.slots, activityId);
}







// SELECTORS


export function getTotalWorkoutDuration(state, workoutId) {
	let sequence = prepareActivitiesSequenceByWorkoutId(state, workoutId)
	let duration = calculateTotalDuration(sequence);
	return duration;
}

export function prepareActivitiesSequenceByWorkoutId(state, workoutId) {
	const activities = activitiesSelectors.getActivitiesByWorkoutId(state.activities, workoutId);
	let sequence = activities.reduce((acc, activity) => {
		const activitySequence = prepareSingleActivitySequence(activity)
		return acc.concat(activitySequence);
	}, []);
	return sequence;
}



export default connectedRootReducer;
