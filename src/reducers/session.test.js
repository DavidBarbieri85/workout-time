import expect from "expect";
import deepFreeze from "deep-freeze";
import { selectedWorkoutId, addingActivity, isAddingWorkout, editingActivityId, editingWorkoutId, editMode  } from "./session";
import { SELECT_WORKOUT, START_ADD_ACTIVITY, END_ADD_ACTIVITY, START_ADD_WORKOUT, END_ADD_WORKOUT, START_EDIT_ACTIVITY, END_EDIT_ACTIVITY, START_EDIT_WORKOUT, END_EDIT_WORKOUT, TOGGLE_EDIT_MODE } from "./actionTypes";


describe("editMode reducer", () => {
	it("should return the initial state", () => {
		expect(editMode(undefined, {})).toEqual(false);
	});
	it("should handle TOGGLE_EDIT_MODE", () => {
		const stateBefore = false;
		const action = {
			type: TOGGLE_EDIT_MODE
		};
		const stateAfter = true;
		deepFreeze(stateAfter);

		expect(editMode(stateBefore, action)).toEqual(stateAfter);
	});
});

describe("selectedWorkoutId reducer", () => {
	it("should return the initial state", () => {
		expect(selectedWorkoutId(undefined, {})).toEqual(null);
	});
	it("should handle SELECT_WORKOUT", () => {
		const stateBefore = null;
		const action = {
			type: SELECT_WORKOUT,
			id: 1
		};
		const stateAfter = 1;
		deepFreeze(stateAfter);

		expect(selectedWorkoutId(stateBefore, action)).toEqual(stateAfter);
	});
});

describe("addingActivity reducer", () => {
	it("should return the initial state", () => {
		expect(addingActivity(undefined, {})).toEqual(null);
	});
	it("should handle START_ADD_ACTIVITY", () => {
		const stateBefore = null;
		const action = {
			type: START_ADD_ACTIVITY,
			activityType: "Rest",
			workoutId: 2
		};
		const stateAfter = {type:"Rest", workoutId: 2};
		deepFreeze(stateAfter);

		expect(addingActivity(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle END_ADD_ACTIVITY", () => {
		const stateBefore = "Rest";
		const action = {
			type: END_ADD_ACTIVITY
		};
		const stateAfter = null;
		deepFreeze(stateBefore);

		expect(addingActivity(stateBefore, action)).toEqual(stateAfter);
	});
});



describe("isAddingWorkout reducer", () => {
	it("should return the initial state", () => {
		expect(isAddingWorkout(undefined, {})).toEqual(false);
	});
	it("should handle START_ADD_WORKOUT", () => {
		const stateBefore = false;
		const action = {
			type: START_ADD_WORKOUT
		};
		const stateAfter = true;
		deepFreeze(stateAfter);

		expect(isAddingWorkout(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle END_ADD_WORKOUT", () => {
		const stateBefore = true;
		const action = {
			type: END_ADD_WORKOUT
		};
		const stateAfter = false;
		deepFreeze(stateAfter);

		expect(isAddingWorkout(stateBefore, action)).toEqual(stateAfter);
	});
});



describe("editingActivityId reducer", () => {
	it("should return the initial state", () => {
		expect(editingActivityId(undefined, {})).toEqual(null);
	});
	it("should handle START_EDIT_ACTIVITY", () => {
		const stateBefore = false;
		const action = {
			type: START_EDIT_ACTIVITY,
			id: 1
		};
		const stateAfter = 1;
		deepFreeze(stateAfter);

		expect(editingActivityId(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle END_EDIT_ACTIVITY", () => {
		const stateBefore = 1;
		const action = {
			type: END_EDIT_ACTIVITY
		};
		const stateAfter = null;
		deepFreeze(stateBefore);

		expect(editingActivityId(stateBefore, action)).toEqual(stateAfter);
	});
});




describe("editingWorkoutId reducer", () => {
	it("should return the initial state", () => {
		expect(editingWorkoutId(undefined, {})).toEqual(null);
	});
	it("should handle START_EDIT_WORKOUT", () => {
		const stateBefore = false;
		const action = {
			type: START_EDIT_WORKOUT,
			id: 1
		};
		const stateAfter = 1;
		deepFreeze(stateAfter);

		expect(editingWorkoutId(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle END_EDIT_WORKOUT", () => {
		const stateBefore = 1;
		const action = {
			type: END_EDIT_WORKOUT
		};
		const stateAfter = null;
		deepFreeze(stateBefore);

		expect(editingWorkoutId(stateBefore, action)).toEqual(stateAfter);
	});
});



