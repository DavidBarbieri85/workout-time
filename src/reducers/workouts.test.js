import expect from "expect";
import deepFreeze from "deep-freeze";
import { allIds, byId, orderedIds } from "./workouts";
import {
	ADD_WORKOUT,
	EDIT_WORKOUT,
	DELETE_WORKOUT,
	REORDER_WORKOUTS,
	REORDER_ACTIVITIES,
	ADD_ACTIVITY,
	DELETE_ACTIVITY
} from "./actionTypes";

describe("allIds reducer", () => {
	it("should return the initial state", () => {
		expect(allIds(undefined, {})).toEqual([]);
	});
	it("should handle ADD_WORKOUT", () => {
		const stateBefore = [];
		const action = {
			type: ADD_WORKOUT,
			id: 1,
			payload: {
				name: "Workout 1",
				description: "description"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle EDIT_WORKOUT", () => {
		const stateBefore = [1];
		const action = {
			type: EDIT_WORKOUT,
			id: 1,
			payload: {
				name: "Workout 1 after",
				description: "description 1 after"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_WORKOUT", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: DELETE_WORKOUT,
			id: 2
		};
		const stateAfter = [1, 3];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
});

describe("byId reducer", () => {
	it("should return the initial state", () => {
		expect(byId(undefined, {})).toEqual({});
	});
	it("should handle ADD_WORKOUT", () => {
		const stateBefore = {};
		const action = {
			type: ADD_WORKOUT,
			id: 1,
			payload: {
				name: "Workout 1",
				description: "description"
			}
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Workout 1",
				description: "description"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle EDIT_WORKOUT", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Workout 1",
				description: "description"
			}
		};
		const action = {
			type: EDIT_WORKOUT,
			id: 1,
			payload: {
				name: "Workout 1 after",
				description: "description 1 after"
			}
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Workout 1 after",
				description: "description 1 after"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_WORKOUT", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Workout 1"
			},
			2: {
				id: 2,
				name: "Workout 2"
			},
			3: {
				id: 3,
				name: "Workout 3"
			}
		};
		const action = {
			type: DELETE_WORKOUT,
			id: 2
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Workout 1"
			},
			3: {
				id: 3,
				name: "Workout 3"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle ADD_ACTIVITY", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Workout 1",
				activities: [1, 2, 3]
			},
			2: {
				id: 2,
				name: "Workout 2",
				activities: [4, 5, 6]
			}
		};
		const action = {
			type: ADD_ACTIVITY,
			workoutId: 1,
			id: 9
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Workout 1",
				activities: [1, 2, 3, 9]
			},
			2: {
				id: 2,
				name: "Workout 2",
				activities: [4, 5, 6]
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle DELETE_ACTIVITY", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Workout 1",
				activities: [1, 2, 3]
			},
			2: {
				id: 2,
				name: "Workout 2",
				activities: [4, 5, 6]
			}
		};
		const action = {
			type: DELETE_ACTIVITY,
			workoutId: 1,
			id: 2
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Workout 1",
				activities: [1, 3]
			},
			2: {
				id: 2,
				name: "Workout 2",
				activities: [4, 5, 6]
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle REORDER_ACTIVITIES", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Workout 1",
				activities: [1, 2, 3, 4]
			},
			2: {
				id: 2,
				name: "Workout 2",
				activities: [5, 6, 7, 8]
			},
			3: {
				id: 3,
				name: "Workout 3",
				activities: []
			}
		};
		const action = {
			type: REORDER_ACTIVITIES,
			workoutId: 1,
			activityIdToMove: 2,
			from: 1,
			to: 3
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Workout 1",
				activities: [1, 3, 4, 2]
			},
			2: {
				id: 2,
				name: "Workout 2",
				activities: [5, 6, 7, 8]
			},
			3: {
				id: 3,
				name: "Workout 3",
				activities: []
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
});

describe("orderedIds reducer", () => {
	it("should return the initial state", () => {
		expect(orderedIds(undefined, {})).toEqual([]);
	});
	it("should handle REORDER_WORKOUTS", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: REORDER_WORKOUTS,
			id: 2,
			from: 1,
			to: 2
		};
		const stateAfter = [1, 3, 2];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(orderedIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle ADD_WORKOUT", () => {
		const stateBefore = [];
		const action = {
			type: ADD_WORKOUT,
			id: 1,
			payload: {
				name: "Workout 1",
				description: "description"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(orderedIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_WORKOUT", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: DELETE_WORKOUT,
			id: 2
		};
		const stateAfter = [1, 3];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(orderedIds(stateBefore, action)).toEqual(stateAfter);
	});
});
