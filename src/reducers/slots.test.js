import expect from "expect";
import deepFreeze from "deep-freeze";
import { allIds, byId } from "./slots";
import {ADD_SLOT,EDIT_SLOT,DELETE_SLOT,DELETE_SLOTS} from "./actionTypes"


describe("allIds reducer", () => {
	it("should return the initial state", () => {
		expect(allIds(undefined, {})).toEqual([]);
	});
	it("should handle ADD_SLOT", () => {
		const stateBefore = [];
		const action = {
			type: ADD_SLOT,
			id: 1,
			payload: {
				name: "Slot 1",
				description: "description"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle EDIT_SLOT", () => {
		const stateBefore = [1];
		const action = {
			type: EDIT_SLOT,
			id: 1,
			payload: {
				name: "Slot 1 after",
				description: "description 1 after"
			}
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_SLOT", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: DELETE_SLOT,
			id: 2
		};
		const stateAfter = [1, 3];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle DELETE_SLOTS", () => {
		const stateBefore = [1, 2, 3];
		const action = {
			type: DELETE_SLOTS,
			ids: [2,3]
		};
		const stateAfter = [1];
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(allIds(stateBefore, action)).toEqual(stateAfter);
	});
});

describe("byId reducer", () => {
	it("should return the initial state", () => {
		expect(byId(undefined, {})).toEqual({});
	});
	it("should handle ADD_SLOT", () => {
		const stateBefore = {};
		const action = {
			type: ADD_SLOT,
			id: 1,
			payload: {
				name: "Slot 1",
				description: "description"
			}
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Slot 1",
				description: "description"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle EDIT_SLOT", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Slot 1",
				description: "description"
			}
		};
		const action = {
			type: EDIT_SLOT,
			id: 1,
			payload: {
				name: "Slot 1 after",
				description: "description 1 after"
			}
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Slot 1 after",
				description: "description 1 after"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});
	it("should handle DELETE_SLOT", () => {
		const stateBefore = {
			1: {
				id: 1,
				name: "Slot 1"
			},
			2: {
				id: 2,
				name: "Slot 2"
			},
			3: {
				id: 3,
				name: "Slot 3"
			}
		};
		const action = {
			type: DELETE_SLOT,
			id: 2
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Slot 1"
			},
			3: {
				id: 3,
				name: "Slot 3"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});

	it("should handle DELETE_SLOTS", () => {

		const stateBefore = {
			1: {
				id: 1,
				name: "Slot 1"
			},
			2: {
				id: 2,
				name: "Slot 2"
			},
			3: {
				id: 3,
				name: "Slot 3"
			}
		};
		const action = {
			type: DELETE_SLOTS,
			ids: [2,3]
		};
		const stateAfter = {
			1: {
				id: 1,
				name: "Slot 1"
			}
		};
		deepFreeze(stateBefore);
		deepFreeze(stateAfter);

		expect(byId(stateBefore, action)).toEqual(stateAfter);
	});

});
