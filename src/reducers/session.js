import { combineReducers } from "redux";
import {
	TOGGLE_EDIT_MODE,
	SELECT_WORKOUT,
	START_ADD_ACTIVITY,
	END_ADD_ACTIVITY,
	START_ADD_WORKOUT,
	END_ADD_WORKOUT,
	START_EDIT_WORKOUT,
	END_EDIT_WORKOUT,
	START_EDIT_ACTIVITY,
	END_EDIT_ACTIVITY
} from "./actionTypes";

export const editMode = (state = false, action) => {
	switch (action.type) {
		case TOGGLE_EDIT_MODE:
			return !state;
		default:
			return state;
	}
};

export const selectedWorkoutId = (state = null, action) => {
	switch (action.type) {
		case SELECT_WORKOUT:
			return action.id;
		default:
			return state;
	}
};

export const addingActivity = (state = null, action) => {
	switch (action.type) {
		case START_ADD_ACTIVITY:
			return {type:action.activityType, workoutId: action.workoutId};
		case END_ADD_ACTIVITY:
			return null;
		default:
			return state;
	}
};

export const isAddingWorkout = (state = false, action) => {
	switch (action.type) {
		case START_ADD_WORKOUT:
			return true;
		case END_ADD_WORKOUT:
			return false;
		default:
			return state;
	}
};

export const editingWorkoutId = (state = null, action) => {
	switch (action.type) {
		case START_EDIT_WORKOUT:
			return action.id;
		case END_EDIT_WORKOUT:
			return null;
		default:
			return state;
	}
};

export const editingActivityId = (state = null, action) => {
	switch (action.type) {
		case START_EDIT_ACTIVITY:
			return action.id;
		case END_EDIT_ACTIVITY:
			return null;
		default:
			return state;
	}
};

export const session = combineReducers({ selectedWorkoutId, addingActivity, isAddingWorkout, editingWorkoutId, editingActivityId, editMode});

// SELECTORS ------------------------------

export default session;
