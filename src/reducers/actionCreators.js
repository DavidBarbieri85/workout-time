import { v4 } from "uuid";

import { ADD_WORKOUT, EDIT_WORKOUT, DELETE_WORKOUT, REORDER_WORKOUTS, REORDER_ACTIVITIES } from "./actionTypes";
import { ADD_ACTIVITY, EDIT_ACTIVITY, DELETE_ACTIVITY, DELETE_ACTIVITIES } from "./actionTypes";
import {
	TOGGLE_EDIT_MODE,
	SELECT_WORKOUT,
	START_ADD_ACTIVITY,
	START_EDIT_ACTIVITY,
	END_ADD_ACTIVITY,
	END_EDIT_ACTIVITY,
	START_ADD_WORKOUT,
	START_EDIT_WORKOUT,
	END_ADD_WORKOUT,
	END_EDIT_WORKOUT
} from "./actionTypes";
import { ADD_SLOT, EDIT_SLOT, DELETE_SLOT, DELETE_SLOTS } from "./actionTypes";

// WORKOUTS

export const addWorkout = payload => ({
	type: ADD_WORKOUT,
	id: v4(),
	payload
});

export const editWorkout = payload => ({
	type: EDIT_WORKOUT,
	id: payload.id,
	payload
});

export const deleteWorkout = id => ({
	type: DELETE_WORKOUT,
	id
});

export const reorderWorkouts = (id, from, to) => ({
	type: REORDER_WORKOUTS,
	id,
	from,
	to
});

export const reorderActivities = (activityIdToMove, from, to, workoutId) => ({
	type: REORDER_ACTIVITIES,
	workoutId,
	activityIdToMove,
	from,
	to
});

// ACTIVITIES

export const addActivity = payload => ({
	type: ADD_ACTIVITY,
	id: v4(),
	workoutId: payload.workoutId,
	payload
});

export const editActivity = payload => ({
	type: EDIT_ACTIVITY,
	id: payload.id,
	payload
});

export const deleteActivity = (workoutId, id) => ({
	type: DELETE_ACTIVITY,
	workoutId,
	id
});

export const deleteActivities = ids => ({
	type: DELETE_ACTIVITIES,
	ids
});

// SLOTS

export const addSlot = payload => ({
	type: ADD_SLOT,
	id: v4(),
	workoutId: payload.workoutId,
	payload
});

export const editSlot = payload => ({
	type: EDIT_SLOT,
	id: payload.id,
	payload
});

export const deleteSlot = (activiyId, id) => ({
	type: DELETE_SLOT,
	activiyId,
	id
});

export const deleteSlots = ids => ({
	type: DELETE_SLOTS,
	ids
});

//UI

export const toggleEditMode = () => ({
	type: TOGGLE_EDIT_MODE
});

export const selectWorkout = id => ({
	type: SELECT_WORKOUT,
	id
});

export const startAddActivity = (activityType, workoutId) => ({
	type: START_ADD_ACTIVITY,
	activityType,
	workoutId
});

export const startEditActivity = id => ({
	type: START_EDIT_ACTIVITY,
	id
});

export const endAddActivity = () => ({
	type: END_ADD_ACTIVITY
});

export const endEditActivity = () => ({
	type: END_EDIT_ACTIVITY
});

export const startAddWorkout = () => ({
	type: START_ADD_WORKOUT
});

export const startEditWorkout = id => ({
	type: START_EDIT_WORKOUT,
	id
});

export const endAddWorkout = () => ({
	type: END_ADD_WORKOUT
});

export const endEditWorkout = () => ({
	type: END_EDIT_WORKOUT
});
