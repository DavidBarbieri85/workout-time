import { combineReducers } from "redux";
import { ADD_SLOT, EDIT_SLOT, DELETE_SLOT, DELETE_SLOTS, DELETE_ACTIVITY,ADD_ACTIVITY } from "./actionTypes";

const slot = (state = {}, action) => {
	switch (action.type) {
		case ADD_SLOT:
		case EDIT_SLOT:
			return {
				...state,
				id: action.id,
				...action.payload
			};
		default:
			return state;
	}
};

export const allIds = (state = [], action) => {
	switch (action.type) {
		case ADD_SLOT:
			return [...state, action.id];

		case DELETE_SLOT:
			const indexOfRemoving = state.indexOf(action.id);
			return [...state.slice(0, indexOfRemoving), ...state.slice(indexOfRemoving + 1)];
		case DELETE_SLOTS:
			return state.filter(id => !action.ids.includes(id));
		default:
			return state;
	}
};

export const byId = (state = {}, action) => {
	switch (action.type) {
		case ADD_SLOT:
		case EDIT_SLOT:
			return {
				...state,
				[action.id]: slot(state[action.id], action)
			};
		case DELETE_SLOT:
			const { [action.id]: value, ...withoutKey } = state;
			return withoutKey;
		case DELETE_SLOTS:
			let newState = action.ids.reduce((iterState, id) => {
				const { [id]: value, ...withoutKey } = iterState;
				return withoutKey;
			}, state);
			return newState;
		default:
			return state;
	}
};

export const idsByActivityId = (state = {}, action) => {
	switch (action.type) {
		case ADD_SLOT:
		case DELETE_SLOT:
			//DANGEROUS
			return { ...state, [action.activityId]: allIds(state[action.activityId], action) };
		case ADD_ACTIVITY:
			return {...state,
				[action.id]: allIds([],action)
			}
		case DELETE_ACTIVITY:
			const { [action.activityId]: value, ...withoutKey } = state;
			return withoutKey;
		default:
			return state;
	}
};

const activities = combineReducers({ byId, allIds, idsByActivityId });

// SELECTORS ------------------------------

export const getSlotsByIds = (state, ids) => {
	return ids.map(id => {
		return state.byId[id];
	});
};

export const getSlotsByActivityId = (state, activityId) => {
	return state.idsByActivityId[activityId].map(id => {
		return state.byId[id];
	});
};

// to be moved

/* export function prepareActivitiesSequence(store) {
	let sequence = store.reduce((acc, activity) => {
		const activitySequence = prepareSingleActivitySequence(activity);
		return acc.concat(activitySequence);
	}, []);

	return sequence;
}

export function prepareSingleActivitySequence(activity) {
	let activitySequence = [];
	if (!activity.numSeries) {
		activitySequence.push({ duration: activity.duration, name: activity.name, description: activity.description, type: "rest" });
		return activitySequence;
	}
	let numberOfSlots = activity.numSeries ? activity.numSeries * 2 - 1 : 1;
	numberOfSlots = activity.restAfter > 0 ? numberOfSlots + 1 : numberOfSlots;

	for (let i = 1; i <= numberOfSlots; i++) {
		let thisSlotDuration = i % 2 !== 0 ? activity.duration : activity.restBetween;
		activitySequence.push({
			duration: thisSlotDuration,
			name: activity.name,
			description: activity.description,
			type: i % 2 !== 0 ? "exercise" : "rest"
		});
	}
	return activitySequence;
} */

export default activities;
