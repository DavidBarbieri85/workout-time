import React from "react";
import {Route} from "react-router"
import Header from "../components/header";
import Sidebar from "../components/sidebar";
import MainContent from "../components/mainContent";


import "../css/index.css";


const Home = () => {
	return (
		<div className="page-container">
		<Header />
			<Sidebar />
			<div className="main-content">
				<Route exact path="/editor/workout/:id" component={MainContent} />
			</div>
		</div>
	);
};

export default Home;
