import React from "react";
import Countdown from "../components/countdown/coundown";
import SequenceSlot from "../components/sequenceSlot";
import { connect } from "react-redux";
import {push} from "connected-react-router"
import { getWorkoutById, prepareActivitiesSequenceByWorkoutId, getTotalWorkoutDuration } from "../reducers";

import "../css/timer.css";

const mapStateToProps = (
	state,
	{
		match: {
			params: { id }
		}
	}
) => ({
    workout: getWorkoutById(state, id),
	totalDuration: getTotalWorkoutDuration(state, id),
	sequence: prepareActivitiesSequenceByWorkoutId(state, id)
});


export class Timer extends React.Component {
	constructor(props) {
		super(props);
		console.log("Timer constructor props: ");
        console.log(props);
        
        this.onCountdownStart = this.onCountdownStart.bind(this)
        this.onCountdownPause = this.onCountdownPause.bind(this)
        this.onCountdownFinish = this.onCountdownFinish.bind(this)
	}

	componentDidMount() {}

	render() {
		return (
			<div className="timer">
				<div className="workout-recap">
					<h1>{this.props.workout.name}</h1>
					<h2>{this.props.workout.description}</h2>
					<div className="total-duration">{this.props.totalDuration}</div>
				</div>
				<div className="sequence-current-activity">
					<div className="sequence-title">CURRENT</div>
					<SequenceSlot slot={this.props.sequence[0]} />
				</div>
				<div className="contdown-container">
					<Countdown
						onStart={this.onCountdownStart}
						onStop={this.onCountdownPause}
						onFinish={this.onCountdownFinish}
						startTimeInSeconds={this.props.sequence[0].duration}
						size={"100%"}
						strokeWidth={"10%"}
					/>
				</div>
				<div className="sequence-next-activity">
					<div className="sequence-title">NEXT</div>
					<SequenceSlot slot={this.props.sequence[1]} />
				</div>
			</div>
		);
	}

	onCountdownStart = () => {
        this.props.push("/timer/workout/" + this.props.workout.id + "/0/running")
    };

	onCountdownPause = () => {
        this.props.push("/timer/workout/" + this.props.workout.id + "/0/paused")
    };

	onCountdownFinish = () => {
        this.props.push("/timer/workout/" + this.props.workoutId +"/finished")
    };
}

export default connect(mapStateToProps,{push})(Timer);
