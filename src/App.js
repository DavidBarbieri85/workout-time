import React from "react";
import { Route } from "react-router";

import Editor from "./pages/editor";
import Timer from "./pages/timer";

const App = () => {
	return (
		<div className="App">
			<Route path="/editor" component={Editor} />
			<Route path="/timer/workout/:id" component={Timer} />
		</div>
	);
};

export default App;
