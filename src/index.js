import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createHashHistory } from "history";
import { ConnectedRouter } from "connected-react-router";


import mockData from "./data/mockData";

import initStore from "./config/store";
import * as localStore from "./localstore"
import "./css/reset.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

const history = createHashHistory();

//localStore.set(mockData)


const initialState = localStore.get()
const store = initStore(history,initialState);

function run() {
	const root = (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<App />
			</ConnectedRouter>
		</Provider>
	);
	ReactDOM.render(root, document.getElementById("root"));
}

function init() {
	run();

	store.subscribe(run);
	store.subscribe( () => {
		localStore.set(store.getState())
	});
}

init();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
