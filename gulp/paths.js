'use strict';
// COMPONENTS
var path = require('path');
var gutil = require('gulp-util');
	//CONST = require('./const');

// PATHS
gutil.log("_****** _dirname ---- " + path.join(__dirname, '.'));
var base_path = path.join(__dirname, '..');
var content_path = path.join(base_path, 'src/content');
var dev_styles_path = path.join(base_path,'src/sass')
//var develop_path = path.join(base_path, 'Develop');
var gulp_path = path.join(base_path, 'gulp');
var tasks_path = path.join(base_path, 'gulp', 'gulp-tasks');
//var json_path = path.join(base_path, 'Configuration', 'Scripts');

// SVG
var svg_path = path.join(content_path, 'icons');



// EXPORTS
exports.base_path = base_path;
exports.content_path = content_path;
exports.gulp_path = gulp_path;
exports.tasks_path = tasks_path;
exports.dev_styles_path = dev_styles_path;
exports.svg_path = svg_path;
