'use strict';
// COMPONENTS
var gulp = require('gulp'),
	path = require('path'),
	fs = require('fs'),
	clean = require('gulp-clean'),
	through = require('through'),
	gutil = require('gulp-util'),
	paths = require('../paths');

// FUNCTIONS

/**
 * Function createMixins
 *
 * @param opts: Object [
 *      styleName: output SCSS file name
 *      styleTemplate: mustache template
 * ]
 *
 * @returns Stream
 */

function createMixins(opts) {
	var tpl = fs.readFileSync(opts.styleTemplate).toString();

	var buffer = [];

	var bufferContents = function(file) {
		gutil.log("---- createMixins ---->>>    "+ path.basename(file.history[0], '.svg'));
		var filepath = file.history[0];
		buffer.push({
			name: path.basename(filepath, '.svg')
		});
	};

	var endStream = function() {
		const mustache = require('mustache');

		this.emit(
			'data',
			new gutil.File({
				contents: new Buffer(
					mustache.render(tpl, {
						files: buffer
					}),
					'utf8'
				),
				path: opts.styleName
			})
		);
		this.emit('end');
	};

	return new through(bufferContents, endStream);
}

function clean() {
	return gulp.src(path.join(paths.svg_path + '/min')).pipe(clean({ force: true }));
}

function minifySvgColored() {
	const svgmin = require('gulp-svgmin');
	return gulp
		.src(path.join(paths.svg_path + '/original/colored/**/*.svg'))
		.pipe(svgmin())
		.pipe(gulp.dest(path.join(paths.svg_path + '/min/colored/')))

		.pipe(
			createMixins({
				styleTemplate: path.join(paths.gulp_path + '/mustache/svgColored.mst'),
				styleName: '_svgColored.scss'
			})
		)
		.pipe(gulp.dest(paths.dev_styles_path + '/shared/'))
		.on('end', function() {
			console.log('Created: ' + paths.dev_styles_path + '/shared/_svgColored.scss');
		});
}

function minifySvg() {
	const replace = require('gulp-replace'),
		svgmin = require('gulp-svgmin');
	return gulp
		.src(path.join(paths.svg_path + '/*.svg')) //'/original/icons/**/*.svg'
		.pipe(svgmin())
		.pipe(replace(/xmlns/g, 'fill="#000" xmlns'))
		.pipe(gulp.dest(path.join(paths.svg_path + '/min/')))
		.pipe(
			createMixins({
				styleTemplate: path.join(paths.gulp_path + '/mustache/svgIcon.mst'),
				styleName: '_svgIcons.scss'
			})
		)
		.pipe(gulp.dest(paths.dev_styles_path + '/shared/'))
		.on('end', function() {
			console.log('Created: ' + paths.dev_styles_path + '/shared/_svgIcons.scss');
		});
}

function prepareSvg() {
	return [clean(), minifySvg(), minifySvgColored()];
}
// EXPORTS
module.exports = prepareSvg;
