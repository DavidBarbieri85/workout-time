$svg-domain: "/";

@function svg_str_replace($string, $search, $replace: '') {
    $string: $string + '';
    $search: $search + '';
    $index: str-index($string, $search);

    @if $index {
        @return str-slice($string, 1, $index - 1) + $replace + svg_str_replace(str-slice($string, $index + str-length($search)), $search, $replace);
    }

    @return $string;
}

@function svg_ends_with($string, $find) {
	@if (str-index($string, $find)==(str-length($string) - str-length($find) + 1)) {
		@return true;
	}
	@else {
		@return false;
	}
}

@function svg_number($value) {
	@if type-of($value)=='number' {
		@return $value;
	}
	@else if type-of($value) !='string' {
		$value: $value+"";
	}
	$result: 0;
	$digits: 0;
	$minus: str-slice($value, 1, 1)=='-';
	$numbers: ('0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9);
	@for $i from if($minus, 2, 1) through str-length($value) {
		$character: str-slice($value, $i, $i);
		@if not (index(map-keys($numbers), $character) or $character=='.') {
			@return if($minus, -$result, $result);
		}
		@if $character=='.' {
			$digits: 1;
		}
		@else if $digits==0 {
			$result: $result * 10 + map-get($numbers, $character);
		}
		@else {
			$digits: $digits * 10;
			$result: $result + map-get($numbers, $character) / $digits;
		}
	}
	@return if($minus, -$result, $result);
}

{{#files}}
@mixin icon-{{name}}($width: 0, $height: 0, $includeSize: true, $color: '000') {
	$color: svg_str_replace($color, '#');

    @if ($height == 0) {
        $height: $width;
    }

	@if ($includeSize) {
       
        width: $width;
        height: $height;

        @if (svg_ends_with($width + "", 'rem') or svg_ends_with($height + "", 'rem')) {
            body.ie & {
                @if (svg_ends_with($width + "", 'rem')) {
                    $px: round(svg_number($width+"") * svg_number($font-size-body))+0px;
                    width: $px;
                }
                @if (svg_ends_with($height + "", 'rem')) {
                    $px: round(svg_number($height+"") * svg_number($font-size-body))+0px;
                    height: $px;
                }
            }
        }
	}
	display: inline-block;
	background-image: url($svg-domain + "./content/icons/min/{{name}}/" + $color);
	background-position: center;
	background-size: contain;
	background-repeat: no-repeat;
}
{{/files}}

{{#files}}
    @function url-{{name}}($color) {
        $color: svg_str_replace($color, '#');
        @return url($svg-domain + "./content/icons/min/{{name}}/" + $color);
    }
{{/files}}
