var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");

gulp.task("styles", function() {
	gulp.src("src/sass/*.scss")
		.pipe(sourcemaps.init())
		.pipe(sass().on("error", sass.logError))
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest("./src/css/"));
});

const prepareSvg = require("./gulp/gulp-tasks/prepareSvg");

// SVG
gulp.task("prepareSvg", function() {
	return prepareSvg();
});

//Watch task
gulp.task("default", ["styles"], function() {
	gulp.watch("src/sass/*.scss", ["styles"]);
});
